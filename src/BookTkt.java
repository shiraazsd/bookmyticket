import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import JDBCUtil.*;

public class BookTkt {

	static int BookTkt(String PnrNo) throws SQLException, IOException, ParseException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println(
				"Enter name,dob(dd-MM-yyyy),journey date(dd-MM-yyyy), doc id as per table, address id as per table ,train id as per table\n");

		System.out.println("docId    typeOfId");
		DocId();
		System.out.println("\ncityid   city");
		;
		CityId();
		System.out.println("\nTrainId  name ");
		TainId();

		String name = br.readLine();

		String dob = br.readLine();

		// java.sql.Date sqlDob = new java.sql.Date(myDate.getTime());

		// String startDate="01-02-2013";
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
		java.util.Date date2 = sdf1.parse(dob);
		java.sql.Date sqlDob = new java.sql.Date(date2.getTime());

		String journeyDate = br.readLine();
		Date myDatedate1 = new SimpleDateFormat("dd-MM-yyyy").parse(journeyDate);
		java.util.Date date3 = sdf1.parse(journeyDate);
		java.sql.Date sqljournDate1 = new java.sql.Date(date3.getTime());

		// SimpleDateFormat dateFormat = new SimpleDateFormat(
		// "yyyy-MM-dd hh:mm:ss");

		// Date parsedTimeStamp = dateFormat.parse(journeyTime);

		// Timestamp journeyTime1 = new Timestamp(parsedTimeStamp.getTime());

		int docId = Integer.parseInt(br.readLine());

		int cityId = Integer.parseInt(br.readLine());

		int trainId = Integer.parseInt(br.readLine());
		// Billing.Billing1(trainId, 4000F);

		String status = "cnf";

		insertPasseng(name, sqlDob, docId, cityId, trainId, sqljournDate1, status, PnrNo);

		return trainId;
	}

	static int insertPasseng(String name, Date dob, int docId, int cityId, int trainId, Date journeyDate1,
			String status, String PnrNo) throws SQLException {

		// Connection con = null;
		// PreparedStatement ps = null;
		// ResultSet rs3 = null;
		// Statement stmt3 = null;

		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");
		// con = JDBCUtil.getOracleConnection();
		PreparedStatement ps = con.prepareStatement("insert into passeng_res values(bookTkt.nextval,?,?,?,?)");

		// ps.setInt(1,id);
		ps.setString(1, name);
		ps.setDate(2, (java.sql.Date) dob);
		ps.setInt(3, docId);
		ps.setInt(4, cityId);
		// ps.setInt(5,id);
		int i = ps.executeUpdate();
		// return i;
		PreparedStatement ps1 = con
				.prepareStatement("insert into reserv_res values(reservseq.nextval, bookTkt.currval ,?,?,?,?,?)");

		// ps1.setString(1, bookTkt.nextval);
		// ps1.setInt(1, trainN);

		// ps1.setDate(3, (java.sql.Date) dob);

		ps1.setInt(1, trainId);
		ps1.setTimestamp(2, getCurrentTimeStamp());
		ps1.setDate(3, (java.sql.Date) journeyDate1);
		ps1.setString(4, status);
		ps1.setString(5, PnrNo);
		int j = ps1.executeUpdate();

		Statement stmt3 = con.createStatement();

		ResultSet rs3 = stmt3.executeQuery(" select train_no from traintable_Res where id=" + trainId);
		while (rs3.next()) {

			int trainNo = rs3.getInt("train_no");
			// String type = rs3.getString(2);

			// System.out.println(" "+docID+" "+type);

			Timestamp ts = new Timestamp(System.currentTimeMillis());
			Date date = new Date(ts.getTime());

			java.sql.Date sqlDate = new java.sql.Date(date.getTime());
			// System.out.println(date);
			// Billing.Billing1(2000F);
			// Seats.seats(sqlDate, trainNo, 6);
		}
		// JDBCUtil.cleanup(rs3, stmt3, con);
		// JDBCUtil.cleanup(ps, con);
		return j;
	}

	// to get current timestamp
	private static java.sql.Timestamp getCurrentTimeStamp() {

		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());

	}

	// to get docId_res table

	static void DocId() throws SQLException {

		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");
		// Connection con = null;
		// PreparedStatement ps = null;
		// ResultSet rs3 = null;
		// Statement stmt3 = null;
		// con = JDBCUtil.getOracleConnection();

		Statement stmt3 = con.createStatement();
		ResultSet rs3 = stmt3.executeQuery(" select id,type from docid_Res");
		while (rs3.next()) {

			int docID = rs3.getInt(1);
			String type = rs3.getString(2);

			System.out.println("  " + docID + "     " + type);
		}
		// JDBCUtil.cleanup(rs3, stmt3, con);
	}

	// get CityId_res table
	static void CityId() throws SQLException {

		// Connection con = null;
		// PreparedStatement ps = null;
		// ResultSet rs3 = null;
		// Statement stmt3 = null;
		// con = JDBCUtil.getOracleConnection();
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");
		Statement stmt3 = con.createStatement();
		ResultSet rs3 = stmt3.executeQuery(" select id ,address from address_Res");
		while (rs3.next()) {

			int id = rs3.getInt(1);
			String addressID = rs3.getString(2);

			System.out.println(" " + id + "     " + addressID);
		}
		// JDBCUtil.cleanup(rs3, stmt3, con);
	}

	static void TainId() throws SQLException {

		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");
		// Connection con = null;
		// PreparedStatement ps = null;
		// ResultSet rs3 = null;
		// Statement stmt3 = null;
		// con = JDBCUtil.getOracleConnection();
		Statement stmt3 = con.createStatement();
		ResultSet rs3 = stmt3.executeQuery(" select id ,trainname from traintable_Res");
		while (rs3.next()) {

			int id = rs3.getInt(1);
			String addressID = rs3.getString(2);

			System.out.println(" " + id + "     " + addressID);

		}

		// JDBCUtil.cleanup(rs3, stmt3, con);

	}

}
