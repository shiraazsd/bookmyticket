import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CancelTkt {

	static void Delete(int passengerId, int ReservationId) throws SQLException {

		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");

		PreparedStatement ps = con.prepareStatement("Delete  from passeng_res where id=?");
		ps.setInt(1, passengerId);
		// TODO:Always hover over the yellow eclipse warnings
		// remove any unused variable declarations
		ps.executeUpdate();

		PreparedStatement ps1 = con.prepareStatement("Delete  from reserv_res where id=?");
		ps1.setInt(1, ReservationId);

		ps1.executeUpdate();
	}

}
