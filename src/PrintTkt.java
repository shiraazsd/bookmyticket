import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class PrintTkt {

	static boolean CheckRecordPasseng(int id, String pnrNo) throws SQLException {

		boolean cond;
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "hello786");
		PreparedStatement ps = con.prepareStatement("select * from tickettable where Pnr_no=?");

		ps.setString(1, pnrNo);
		ps.executeUpdate();
		ResultSet rs = ps.executeQuery();

		if (rs.next()) {
			cond = true;
		} else
			cond = false;

		return cond;

	}

	static void printTkt(String pnrNo) throws ClassNotFoundException, SQLException {

		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");

		String qry = "select trainname ,passengername,trainid,journeydate,status, pnrno ,srcstation ,deststation  from tickettable where Pnrno=?";
		PreparedStatement ps = con.prepareStatement(qry);
		ps.setString(1, pnrNo);

		ResultSet rs = ps.executeQuery();

		String output = "";
		System.out.println(
				"trainName     nameOfPAsseng    TrainNo        joourneyDate        Status         PnrNo              srcStn     DestStn");
		// TODO:Use arraylist everywhere
		while (rs.next()) {

			output = output + "\n " + rs.getString("trainname") + "         " + rs.getString("passengername")
					+ "          " + rs.getInt("trainid") + "          " + rs.getDate("journeydate") + "          "
					+ rs.getString("status") + "             " + rs.getString("pnrno") + "      "
					+ rs.getString("srcstation") + "        " + rs.getString("deststation");

		}

		System.out.print(output);
	}
}
