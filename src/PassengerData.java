import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import JDBCUtil.*;

public class PassengerData {

	static boolean CheckRecord(int id) throws SQLException {

		boolean cond;
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");
		PreparedStatement ps = con.prepareStatement("select * from passengData where id=?");

		ps.setInt(1, id);
		ps.executeUpdate();
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			cond = true;
		} else
			cond = false;

		return cond;
	}

	static int passengerData(String name, String password, String email, Long phoneNo) throws SQLException {

		// Connection con = null;
		// ResultSet rs3 = null;
		// PreparedStatement ps = null;
		// con = JDBCUtil.getOracleConnection();
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "hello786");
		PreparedStatement ps = con.prepareStatement("insert into passengData values(passengDataseq.nextval,?,?,?,?)");

		ps.setString(1, name);
		ps.setString(2, password);
		ps.setString(3, email);
		ps.setLong(4, phoneNo);

		int i = ps.executeUpdate();
		// JDBCUtil.cleanup(ps, con);
		return i;
	}

	static void Delete(int passengId) throws SQLException {

		boolean condn = CheckRecord(passengId);
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "hello786");

		while (condn != false) {
			PreparedStatement ps = con.prepareStatement("Delete  from passengdata where id=?");
			ps.setInt(1, passengId);
			ps.executeUpdate();

			System.out.println("Tkt cancelled successfully");
			break;
		}

		if (condn != true)
			System.out.println("No record found");
	}
}
