package Test;

import java.util.List;

import com.bookmytkt.InterfaceDao.DocIdDao;
import com.bookmytkt.Newdao.CityDaoImpl;
import com.bookmytkt.Newdao.DocIdDaoImpl;
import com.bookmytkt.dto.City;
import com.bookmytkt.dto.DocId;

public class TestDocID {

	public static void main(String[] args) {
		int x = 0;
    	//add billing
    	DocIdDao dao  = new DocIdDaoImpl();
    	List<Object> list = dao.getAll();
		
    	DocId c = new DocId("college id");
	    x = dao.add(c);
		System.out.println(x);
		
		list = dao.getAll();
		for(Object city:list)
		System.out.println(city);
		
		
		//update billing(depends on parent)
		c =  new DocId(1,"voter id");
		 x = dao.update(c);
   		System.out.println(x);
		
   	//delete billing
   			x  = dao.delete(234);
   			System.out.println(x);
   			
   			//billing by id
   			 list = dao.getIdByType("adhar");
   			for(Object docID:list)
   				System.out.println(docID);
   		
   			
   		
   		

	}

}
