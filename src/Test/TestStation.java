package Test;

import java.sql.SQLException;
import java.util.List;

import com.bookmytkt.InterfaceDao.BillingDao;
import com.bookmytkt.InterfaceDao.StationDao;
import com.bookmytkt.Newdao.StationDaoImpl;
import com.bookmytkt.dao.BillingDaoImpl;
import com.bookmytkt.dto.Billing;
import com.bookmytkt.dto.Station;

public class TestStation {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		boolean x = false;
    	//add billing
    	StationDao dao  = new StationDaoImpl();
    	List<Object> list = dao.getAll();
		
    	Station st  = new Station("hyd", 1014);
	    x = dao.add(st);
		System.out.println(x);
		
		list = dao.getAll();
		for(Object sta:list)
		System.out.println(sta);
		
		
		//update billing(depends on parent)
		 st  = new Station(1,"hyd", 1014);
		 x = dao.update(st);
   		System.out.println(x);
		
   	//delete billing
   			x  = dao.delete(904);
   			System.out.println(x);
   			
   			//billing by id
   			 st  = dao.getstationById(305);
   			System.out.println(st);
   			
   			//get all customers with this stationname
   			st = dao.getStationByStationName("hyd");
   			System.out.println(st);
   			
   			//get all customer with this cityid
   			st = dao.getStationByCityId(1012);
   			
   			System.out.println(st);
	}
}
