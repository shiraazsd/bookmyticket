package Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bookmytkt.InterfaceDao.ReservationDao;
import com.bookmytkt.Newdao.PassengerDaoImpl;
import com.bookmytkt.Newdao.ReservationDaoImpl;
import com.bookmytkt.dto.Passenger;
import com.bookmytkt.dto.Reservation;
import com.bookmytkt.utility.PnrNoGenerator;
//rectify error
public class TestReservation {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		
		int x = 0;
    	//add billing
    	ReservationDao dao  = new ReservationDaoImpl();
    	List<Object> list = dao.getAll();
		Reservation c = null;
    	
    	 c = new Reservation(402,202,"23-06-2018","29-06-2018","cnf",PnrNoGenerator.PNRGen());
	    x = dao.add(c);
		System.out.println(x);
		
		list = dao.getAll();
		for(Object obj:list)
		System.out.println(obj);
		
		
		//update billing(depends on parent)
		c =  new Reservation(207,403,202,"23-11-2018","29-11-2018","cnf");
		 x = dao.update(c);
   		System.out.println(x);
		
   	//delete billing
   			x  = dao.delete(412);
   			System.out.println(x);
   			
   			//billing by id
   			List<Reservation> al = new ArrayList();
   			 al = dao.getReservationByPassengerId(402);
   			for(Reservation l:al)
   				System.out.println(l);
   			
   			c = dao.getReservationById(209);
   			System.out.println(c);
   		
  			 al = dao.getReservationByTrainId(201);
  			for(Reservation l:al)
  				System.out.println(l);
  			
  			 al = dao.getReservationByJourneyDate("23-06-2018");
   			for(Reservation l:al)
   				System.out.println(l);
   			
   			al = dao.getReservationByReservationDate("23-06-2018");
   			for(Reservation l:al)
   				System.out.println(l);
   			
   			
   			
   		 c = dao.getReservationByPnrNo("354-9796639");			
				System.out.println(c);
   			

	}

}
