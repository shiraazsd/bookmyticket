package Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bookmytkt.InterfaceDao.SeatsDao;
import com.bookmytkt.Newdao.DocIdDaoImpl;
import com.bookmytkt.Newdao.SeatsDaoImpl;
import com.bookmytkt.dto.*;
import com.bookmytkt.dto.DocId;

public class TestSeats {

	public static void main(String[] args) throws SQLException {
		int x = 0;
    	//add billing
    	SeatsDao dao  = new SeatsDaoImpl();
    	List<Object> list = dao.getAll();
		
    	Seats c = new Seats("16-12-2018",13412,8);
	    x = dao.add(c);
		System.out.println(x);
		
		list = dao.getAll();
		for(Object city:list)
		System.out.println(city);
		
		
		//update billing(depends on parent)
		c =  new Seats(1,"23-11-1998",13412,8);
		 x = dao.update(c);
   		System.out.println(x);
		
   	//delete billing
   			x  = dao.delete(10);
   			System.out.println(x);
   			
   			//billing by id
   			List al  = new ArrayList();
   			 al = dao.getSeatsByDateOfBooking("23-11-1998");
   			for(Object docID:al)
   				System.out.println(docID);
   			
  			 al = dao.getSeatsBySeatsBooked(6);
  			for(Object docID:al)
  				System.out.println(docID);
  			
  			 al = dao.getSeatsByTrainNo(12313);
  			for(Object docID:al)
  				System.out.println(docID);
   		
  			 c = dao.getSeatsById(44);  			
  				System.out.println(c);

	}

}
