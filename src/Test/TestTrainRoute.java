package Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bookmytkt.InterfaceDao.TrainRouteDao;
import com.bookmytkt.Newdao.TrainRouteDaoImpl;
import com.bookmytkt.Newdao.TrainTableDaoImpl;
import com.bookmytkt.dto.TrainRoute;
import com.bookmytkt.dto.Traintable;
//correct arrival and departure time
public class TestTrainRoute {

	public static void main(String[] args) throws SQLException {
		
		int x = 0;
    	//add billing
    	TrainRouteDao dao  = new  TrainRouteDaoImpl();
    	List<Object> list = dao.getAll();
		
    	TrainRoute c = new TrainRoute(202,303,"11:40","12:35");
	    x = dao.add(c);
		System.out.println(x);
		
		list = dao.getAll();
		for(Object city:list)
		System.out.println(city);
		
		
		//update billing(depends on parent)
		c =  new TrainRoute(1,202,303,"12:40","12:50");
		 x = dao.update(c);
   		System.out.println(x);
		
   	//delete billing
   			x  = dao.delete(3);
   			System.out.println(x);
   			
   			//billing by id
   			List al  = new ArrayList();
   			 al = dao.getTrainRouteByTrainId(201);
   			for(Object docID:al)
   				System.out.println(docID);
   			
  			 al = dao.getTrainRouteByStationId(301);
  			for(Object docID:al)
  				System.out.println(docID);
  			
  			 al = dao.getTrainRouteByArrivalTime("11:40");
  			for(Object docID:al)
  				System.out.println(docID);
  			
  			al = dao.getTrainRouteByDepartureTime("12:50");
  			for(Object docID:al)
  				System.out.println(docID);
  			
   		
  			 c = dao.getTrainRouteById(21);  			
  				System.out.println(c);
	

	}

}
