package Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bookmytkt.InterfaceDao.TrainTableDao;
import com.bookmytkt.Newdao.SeatsDaoImpl;
import com.bookmytkt.Newdao.TrainTableDaoImpl;
import com.bookmytkt.dto.Seats;
import com.bookmytkt.dto.Traintable;

public class TestTrainTable {

	public static void main(String[] args) throws SQLException {
		int x = 0;
    	//add billing
    	TrainTableDao dao  = new  TrainTableDaoImpl();
    	List<Object> list = dao.getAll();
		
    	Traintable c = new Traintable("shatabdi",13412,800,3);
	    x = dao.add(c);
		System.out.println(x);
		
		list = dao.getAll();
		for(Object city:list)
		System.out.println(city);
		
		
		//update billing(depends on parent)
		c =  new Traintable(1,"duronto",13412,800F,7);
		 x = dao.update(c);
   		System.out.println(x);
		
   	//delete billing
   			x  = dao.delete(101);
   			System.out.println(x);
   			
   			//billing by id
   			List al  = new ArrayList();
   			 al = dao.getTrainTableByNoOfSeats(200);
   			for(Object docID:al)
   				System.out.println(docID);
   			
  			 al = dao.getTrainTableByTrainFare(800F);
  			for(Object docID:al)
  				System.out.println(docID);
  			
  			 al = dao.getTrainTableByTrainName("duronto");
  			for(Object docID:al)
  				System.out.println(docID);
  			
  			al = dao.getTrainTableByTrainNo(13123);
  			for(Object docID:al)
  				System.out.println(docID);
  			
   		
  			 c = dao.getTrainTableById(2);  			
  				System.out.println(c);
	}

}
