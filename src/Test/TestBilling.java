package Test;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bookmytkt.InterfaceDao.*;
import com.bookmytkt.dao.*;
import com.bookmytkt.dto.*;
public class TestBilling {

	public static void main(String[] args) throws SQLException {
		
		
    	int x = 0;
    	//add billing
    	BillingDao dao  = new BillingDaoImpl();
    	List<Object> list = dao.getAll();
		
    	Billing billing1  = new Billing(801,3089.99F);
	    x = dao.add(billing1);
		System.out.println(x);
		
		//update billing(depends on parent)
		Billing billing2 = new Billing(905,801,9089.99F);
		x = dao.update(billing2);
		System.out.println(x);
		
		
		//delete billing
		x  = dao.delete(904);
		System.out.println(x);
		
		//billing by id
		Billing bil = dao.getBillingById(905);
		System.out.println(bil);
		
		List<Billing> al  = new ArrayList();
		//get all customers with this amt
		al = dao.getBillingByAmt(3089.99F);
		for(Object bill:al)
		System.out.println(bill);
		
		//get all customer with this reservationId
		al = dao.getBillingByReservID(905);
		for(Billing bill:al)
		System.out.println(bill);
		
		list = dao.getAll();
		for(Object bill:list)
		System.out.println(bill);
		
		


	}

}
