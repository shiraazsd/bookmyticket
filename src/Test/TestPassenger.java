package Test;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.plaf.SliderUI;

import com.bookmytkt.InterfaceDao.PassengerDao;
import com.bookmytkt.Newdao.DocIdDaoImpl;
import com.bookmytkt.Newdao.PassengerDaoImpl;
import com.bookmytkt.dto.DocId;
import com.bookmytkt.dto.Passenger;

public class TestPassenger {

	public static void main(String[] args) throws SQLException, ParseException {
		// TODO Auto-generated method stub
		boolean x = false;
		
		String dob = "27-11-1987";
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
		java.util.Date datet = sdf1.parse(dob);
		java.sql.Date sqlDob = new java.sql.Date(datet.getTime());

    	//add billing
    	PassengerDao dao  = new PassengerDaoImpl();
    	List<Object> list = dao.getAll();
		Passenger c = null;
    	
    	 c = new Passenger("misah",sqlDob,702,101);
	    x = dao.add(c);
		System.out.println(x);
		
		list = dao.getAll();
		for(Object obj:list)
		System.out.println(obj);
		
		
		//update billing(depends on parent)
		c =  new Passenger(407,"misbah",sqlDob,701,102);
		 x = dao.update(c);
   		System.out.println(x);
		
   	//delete billing
   			x  = dao.delete(412);
   			System.out.println(x);
   			
   			//billing by id
   			List<Passenger> al = new ArrayList();
   			 al = dao.getPassengerByPassengerName("misah");
   			for(Passenger l:al)
   				System.out.println(l);
   			
   			c = dao.getPassengerById(401);
   			System.out.println(c);
   		
  			 al = dao.getPassengerByDOB(sqlDob);
  			for(Passenger l:al)
  				System.out.println(l);
  			
  			 al = dao.getPassengerByDocId(701);
   			for(Passenger l:al)
   				System.out.println(l);
   			
   		 al = dao.getPassengerByAddressId(101);
			for(Passenger l:al)
				System.out.println(l);
	}

}
