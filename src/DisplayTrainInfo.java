import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class DisplayTrainInfo {

	static void Display() throws SQLException, ClassNotFoundException, NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");

		System.out.println("Enter src station ,dest station ,journey date as per the data");
		System.out.println(Station.stationTable());
		int SrcId = Integer.parseInt(br.readLine());
		int DestId = Integer.parseInt(br.readLine());
		String journeyDate = br.readLine();

		trainTkt t = new trainTkt(SrcId, DestId);

		// Class.forName("oracle.jdbc.driver.OracleDriver");
		// Connection
		// con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","hello786");
		// con = JDBCUtil.getOracleConnection();
		String output = "";

		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(
				"select train_no,    trainname ,noofseats,arrivtime,depttime from TrainTable_Res x inner join TrainRoute_res y on x.id = y.trainid  where SRCSTATIONID = "
						+ SrcId + " and DESTSTATIONID =" + DestId);

		Date d1 = null;
		Date d2 = null;
		System.out.println(
				"TrainNo     Train name    noOfSeats      arrivTime             DeptTime     arrivCity   DeptCity      TravelTime");

		while (rs.next()) {

			output = output + " \n " + rs.getInt("train_no") + "       " + rs.getString("trainname") + "            "
					+ rs.getInt("noofseats") + "         " + rs.getTime("arrivtime") + "             "
					+ rs.getTime("depttime") + " ";

		}

		System.out.print(output);

		String output1 = "";

		Statement stmt1 = con.createStatement();
		ResultSet rs1 = stmt1
				.executeQuery("select stationname from Station_Res where ID = " + SrcId + "or id=" + DestId);
		while (rs1.next()) {

			output1 = output1 + "       " + rs1.getString("stationname") + "\t";

		}
		// String date = br.readLine();
		System.out.print(output1);

		Date d = null;
		Date d3 = null;

		Statement stmt3 = con.createStatement();
		ResultSet rs3 = stmt3.executeQuery(" select depttime,arrivtime   from Trainroute_Res where SRCSTATIONID ="
				+ SrcId + " and DESTSTATIONID	 =" + DestId);
		while (rs3.next()) {

			d = rs3.getTime("depttime");
			d3 = rs3.getTime("arrivtime");
			long diff = d.getTime() - d3.getTime();
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffMinutes = diff / (60 * 1000) % 60;
			System.out.println(" " + diffHours + "  hour  " + diffMinutes + "  min");

		}

	}

	static boolean CheckSeatAvailability(int TrainNo, int NoOfTkt) throws ClassNotFoundException, SQLException {

		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");

		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(
				"select train_no,    trainname ,noofseats,arrivtime,depttime from TrainTable_Res x inner join TrainRoute_res y on x.id = y.trainid  where train_no ="
						+ TrainNo);

		while (rs.next()) {

			// output = output +" "+ rs.getInt(1) +" "+ rs.getString(2) +" "+rs.getInt(3)+"
			// "+ rs.getTime(4)+" "+rs.getTime(5)+" ";

			// TODO:use column names instead of column positions everywhere
			if (NoOfTkt > rs.getInt("noofseats")) {
				
				return false;
			}
		}
		return true;
	}
}
