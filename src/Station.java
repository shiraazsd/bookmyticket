
//package info.Misbah;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import JDBCUtil.JDBCUtil;

public class Station {

	static void Station1() throws Exception {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");
		// Connection con = null;
		// ResultSet rs3 = null;
		// PreparedStatement ps = null;
		// con = JDBCUtil.getOracleConnection();

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		do {
			System.out.println("enter id");
			int id = Integer.parseInt(br.readLine());

			System.out.println("enter station name:");
			String stationName = br.readLine();

			System.out.println("enter CityId");
			int cityId = Integer.parseInt(br.readLine());

			int i = InserData1(id, stationName, cityId);// func called

			System.out.println(i + " records affected");
			System.out.println("Do you want to continue: y/n");
			String s = br.readLine();

			if (s.startsWith("n")) {
				break;
			}
		} while (true);

		con.close();
	}

	// Funct to insert data
	static int InserData1(int id, String stationName, int cityid) throws SQLException, ClassNotFoundException {

		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");
		// Connection con = null;
		// ResultSet rs3 = null;
		// PreparedStatement ps = null;
		// con = JDBCUtil.getOracleConnection();

		PreparedStatement ps = con.prepareStatement("insert into station_res values(?,?,?)");

		ps.setInt(1, id);
		ps.setString(2, stationName);
		ps.setInt(3, cityid);
		// ps.setInt(4,RouteId);
		int i = ps.executeUpdate();

		// JDBCUtil.cleanup(ps, con);
		return i;

	}

	public static String stationTable() throws SQLException, ClassNotFoundException {

		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");
		// Connection con = null;
		// ResultSet rs3 = null;
		// Statement stmt3 = null;
		// con = JDBCUtil.getOracleConnection();
		
		

		String output = "";

		Statement stmt = con.createStatement();
		ResultSet rs3 = stmt.executeQuery("select id,stationname from station_res");
		while (rs3.next()) {

			output = output + " " + rs3.getInt("id") + " " + rs3.getString("stationname") + "\n";

		}
		

		// JDBCUtil.cleanup(rs3, stmt3, con);
		return output;
	}
}
