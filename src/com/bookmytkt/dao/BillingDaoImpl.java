package com.bookmytkt.dao;
import java.sql.Connection;
import JDBCUtil.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.bookmytkt.dto.*;
import com.bookmytkt.InterfaceDao.*;

public class BillingDaoImpl implements BillingDao
{
    List<Billing> billings;//list obj  billings created.
    Billing b = new Billing();
    
    
    private static final String DELETE = "delete from billing_res  where id=?";
	private static final String FIND_ALL = "SELECT * FROM billing_res ORDER BY id";
	private static final String FIND_BY_ID = "SELECT * FROM billing_res WHERE id=?";
	private static final String FIND_BY_AMT = "SELECT * FROM billing_res WHERE amt=?";
	private static final String INSERT = "INSERT INTO billing_res VALUES(billingseq.nextval, ?, ?)";
	private static final String UPDATE = "UPDATE billing_res SET reserv_id=?, amt=? WHERE id=?";
	    
    public int addBilling(Billing billing) {
    	
    	int x = 0;
    	  Connection con;
		try {
			con = JDBCUtil.getOracleConnection();
			  PreparedStatement ps = con.prepareStatement(INSERT);
		  		 // Statement stmt = con.createStatement();
		         // ps.setInt(1, billing.getId());
		          ps.setInt(1, billing.getReservationId());
		          ps.setFloat(2, billing.getAmount());		
		          x = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    	return x;
    	  
    	
    	
    }

	@Override
	public List<Billing> getAllBilling() {//returns all the data in the list
		
		Connection con = null;
		PreparedStatement stmt = null;
		List<Billing> list = new ArrayList<>();
		
		try {
			con = JDBCUtil.getOracleConnection();
			stmt = con.prepareStatement(FIND_ALL);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				Billing bil = new Billing();
				bil.setId(rs.getInt("id"));
				bil.setReservationId(rs.getInt("reserv_id"));
				bil.setAmount(rs.getFloat("amt"));
				
				list.add(bil);
			}
		} catch (SQLException e) {
			// e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			
		}
		
		return list;
		//list returned
	}

	
	
	public int  updateBilling(Billing billing) throws SQLException {
		
		int x = 0;
		Connection con;
		
		try {
			con = JDBCUtil.getOracleConnection(); 
			
			PreparedStatement ps = con.prepareStatement(UPDATE);
			ps.setInt(1, billing.getReservationId());
			ps.setFloat(2, billing.getAmount());
			ps.setInt(3, billing.getId());		
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}		
		//JDBCUtil.cleanup(ps, con);
		return x;
	}
	
	public int deleteBilling(int id) throws SQLException {
		int x = 0;
		try {
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(DELETE);
			ps.setInt(1, id);
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x;
			
			//JDBCUtil.cleanup(ps, con);
	}

	@Override
	public Billing getBillingById(int billingId) throws SQLException {
		// TODO Auto-generated method stub
		Billing bil = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
			ps.setInt(1, billingId);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 bil = new Billing();
				bil.setId(rs.getInt("id"));
				bil.setReservationId(rs.getInt("reserv_id"));
				bil.setAmount(rs.getFloat("amt"));	
				
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return bil;
		}

	@Override
	public List <Billing> getBillingByReservID(int ReservId) {
		Billing bil = null;
		ResultSet rs = null;
		List<Billing> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
			ps.setInt(1, ReservId);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 bil = new Billing();
				bil.setId(rs.getInt("id"));
				bil.setReservationId(rs.getInt("reserv_id"));
				bil.setAmount(rs.getFloat("amt"));	
				list.add(bil);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
		}

	

	@Override
	public List<Billing> getBillingByAmt(Float amt) {
		Billing bil = null;
		ResultSet rs = null;
		List<Billing> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_AMT);
			ps.setFloat(1, amt);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 bil = new Billing();
				bil.setId(rs.getInt("id"));
				bil.setReservationId(rs.getInt("reserv_id"));
				bil.setAmount(rs.getFloat("amt"));	
				list.add(bil);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}
   
}
 