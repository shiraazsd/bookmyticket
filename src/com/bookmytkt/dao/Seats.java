package com.bookmytkt.dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import JDBCUtil.JDBCUtil;

public class Seats {

	static int seats(Date bookDate, int trainNo, int seatsBooked) throws SQLException {

		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "hello786");
		// Connection con = null;
		// ResultSet rs3 = null;
		// PreparedStatement ps = null;
		// con = JDBCUtil.getOracleConnection();
		PreparedStatement ps = con.prepareStatement("insert into seats values(seatseq.nextval,?,?,?)");

		ps.setDate(1, (java.sql.Date) bookDate);
		ps.setInt(2, trainNo);
		ps.setInt(3, seatsBooked);
		int i = ps.executeUpdate();

		// JDBCUtil.cleanup(ps, con);
		return i;

	}

}
