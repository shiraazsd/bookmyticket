package com.bookmytkt.dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UpdateSeats {

	public static boolean updateSeats(int noOfBookedTkt, int trainId) throws SQLException, ClassNotFoundException {
		
		Class.forName("oracle.jdbc.driver.OracleDriver");

		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");
		boolean cond = false;

		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(" select noofseats from traintable_Res where id=" + trainId);
		while (rs.next()) {

			int NoOfSeats = rs.getInt("noofseats");
			int SeatsRem = NoOfSeats - noOfBookedTkt;
			if (SeatsRem > 0) {
				PreparedStatement ps = con.prepareStatement("update traintable_res set noofseats=? where id=?");
				ps.setInt(1, SeatsRem);
				ps.setInt(2, trainId);

				ps.executeUpdate();
				// TODO:Make use of ternary operator over here and other places as much as
				// possible
				cond = true;
			} else
				cond = false;
		}

		return cond;
	}

}
