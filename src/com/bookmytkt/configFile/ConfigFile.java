package com.bookmytkt.configFile;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import java.io.FileOutputStream;


//put config file in bin folder to load from class path ,if put in src will display in eclipse
public class ConfigFile {


  public static void main(String[] args) {
	
	Properties prop = new Properties();
	OutputStream output = null;

	try {

		output = new FileOutputStream("config.properties");

		// set the properties value
		prop.setProperty("port", "8080");
		
		prop.setProperty("dbuser", "system");
		prop.setProperty("dbpassword", "password");
		prop.setProperty("DBhost", "localhost");
		prop.setProperty("DBName", "oracle");

		// save properties to project root folder
		prop.store(output, null);

	} catch (IOException io) {
		io.printStackTrace();
	} finally {
		if (output != null) {
			try {
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
  }
}