package com.bookmytkt.application;
import java.io.IOException;
import com.bookmytkt.com.*;

import com.bookmytkt.dao.CancelTkt;
import com.bookmytkt.dao.ChangePassword;
import com.bookmytkt.dao.Station;
import com.bookmytkt.dao.UpdateSeats;
import com.bookmytkt.dao.UserTable;
import com.bookmytkt.com.Billing;
import com.bookmytkt.com.BookTkt;
import com.bookmytkt.com.DisplayTrainInfo;
import com.bookmytkt.com.LogIn;
import com.bookmytkt.com.MobileNumberValidation;
import com.bookmytkt.com.PasswordValidation;
import com.bookmytkt.com.PnrNoGenerator;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;

import com.bookmytkt.utility.*;

import oracle.jdbc.proxy.annotation.GetCreator;

public class Welcome {

	public static void main(String[] args) throws SQLException, IOException, ClassNotFoundException, ParseException {
		// This is a new comment
		// TODO:1)All the class names should start with Capital Letters 2)All the method
		// and variable names should follow camel case naming convention strictly
		// updateProfileDetails(), do not use numbers, special characters
		
		// use 'return' to come oout of a funct and not break
		//put functions into a class with meaningful name
		
		// use private fields in DAO pkg class
		//use get() and set() method
		

		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter 1 to Login,2 to Register");
		int ch = sc.nextInt();

		switch (ch) {

		case 1:
			System.out.println("Enter username and password");
			String username = sc.next();
			String password = sc.next();

			// TODO:
			// 1)Refactor the complete codebase to move all the switch case flow and input
			// calls in main class.
			// 2)The functions should perform there individual task and return the results
			// strictly
			// 3)Any console outputs should be printed in the main strictly
			boolean conditionLogin = LogIn.logIn(username, password);
			if (conditionLogin != false) {
				System.out.println("login successful");

				System.out.println("Enter 1 to viewTrain,  2 for view booking, 3 to change password");

				int viewTrainchoice = sc.nextInt();

				switch (viewTrainchoice) {

				case 1:

					System.out.println("Enter src station ,dest station ,journey date as per the data");
					LinkedHashMap p = Station.getstationIdAndName();
					Set data  = p.entrySet();
					Iterator it = data.iterator();
					while (it.hasNext()) {
						Object obj = it.next();
						Map.Entry entry = (Map.Entry)obj; 
						Object key = entry.getKey();
						Object val  = entry.getValue();
					System.out.println( key+"\t"+val);
						
					}
					
					int srcId = sc.nextInt();
					int destId = sc.nextInt();
					// String journeyDate = br.readLine();
					System.out.println("TrainNo   Train name    noOfSeats   "
							+ "   arrivTime             DeptTime     arrivCity  "
							+ " DeptCity      TravelTime(hh:mm)");
					ArrayList<String> al = new ArrayList<String>();					
					
					
					al = DisplayTrainInfo.Display( srcId, destId);

					for (String obj : al) {
						System.out.print(obj+"  \t    ");
					}

					System.out.println();

					System.out.println("Enter trainNO ,no of tkt to be booked");
					int trainNo = sc.nextInt();
					int noOfTkt = sc.nextInt();

					boolean seatAvailcondition = DisplayTrainInfo.CheckSeatAvailability(trainNo, noOfTkt);
					if (seatAvailcondition == false) {
						System.out.println("Sorry,no more seats available");
						break;
					}

					int trainId = 0;

					String pnrNo = PnrNoGenerator.PNRGen();

					for (int i = 0; i < noOfTkt; i++) {
						// trainId = BookTkt.BookTkt(pnrNo);

						System.out.println("Enter name,dob(dd-MM-yyyy),"
								+ "journey date(dd-MM-yyyy), doc id as per table"
								+ ", address id as per table ,train id as per table\n");

						System.out.println("docId    typeOfId\n");

						LinkedHashMap docsId = new LinkedHashMap<>();
						docsId = BookTkt.getDocId();						
						Set docData  = docsId.entrySet();
						Iterator itx = docData.iterator();
						while (itx.hasNext()) {
							Object obj = itx.next();
							Map.Entry entry = (Map.Entry)obj; 
							Object key = entry.getKey();
							Object val  = entry.getValue();
						System.out.println( key+"\t"+val);
							
						}
						
						
						System.out.println("\ncityid   city\n");
						LinkedHashMap mcityId = new LinkedHashMap<>();
						mcityId = BookTkt.getCityId();
						Set cityData  = mcityId.entrySet();
						Iterator ity = cityData.iterator();
						while (ity.hasNext()) {
							Object obj = ity.next();
							Map.Entry entry = (Map.Entry)obj; 
							Object key = entry.getKey();
							Object val  = entry.getValue();
						System.out.println( key+"\t"+val);							
						}
						
						
						System.out.println("\nTrainId  name \n");
						LinkedHashMap mtrainId = new LinkedHashMap<>();
						mtrainId = BookTkt.getTrainId();
						Set trainData  = mtrainId.entrySet();
						Iterator itz = trainData.iterator();
						while (itz.hasNext()) {
							Object obj = itz.next();
							Map.Entry entry = (Map.Entry)obj; 
							Object key = entry.getKey();
							Object val  = entry.getValue();
						System.out.println( key+"\t"+val);							
						}

						String name = sc.next();
						String dob = sc.next();
						String journeyDate = sc.next();
						
						boolean cond = IsValidDate.isValid(journeyDate);
						if(cond==true) {
							System.out.println("valid date");
						}
						else return;
						
						int docId = sc.nextInt();
						int cityId = sc.nextInt();
						trainId = sc.nextInt();

						String status = "cnf";

						BookTkt.addPassenger(name, dob, docId, cityId, trainId, journeyDate, status, pnrNo);

					}//end of for loop
					
					Billing.billing(trainId, noOfTkt);
						boolean cond = UpdateSeats.updateSeats(noOfTkt, trainId);
						if (cond != false) {
							System.out.println("Tkt booked successfully");
							// trainTkt.updatetrainTkttable(trainNo, trainId, pnrNo);

						} else
							System.out.println("Sorry,No seats available");
					
					break;
					
					//case 2 for viewTrainChoice
				case 2:

					System.out.println("Enter passId,reservId,PnrNo ");
					int passengId = sc.nextInt();
					int reservId = sc.nextInt();

					pnrNo = sc.next();

					System.out.println("Enter 1 to cancel booking,  2 to print tkt");

					int bookingch = sc.nextInt();

					switch (bookingch) {

					case 1:

						CancelTkt.Delete(passengId, reservId);
						break;

					case 2:

						//PrintTkt.printTkt(pnrNo);
						break;
					}

					break;
					
					//case 3 for viewTrainChoice
				case 3:

					System.out.println("Enter userName,password to change");
					String userNameUpdate = sc.next();
					String passwordUpdate = sc.next();
					ChangePassword.changePassword(userNameUpdate, passwordUpdate);
					break;

				}

			} else //else block for if login
				System.out.println("invalid username or password");
			break;
		// TODO:Add validations for username(should be unique), password(use some
		// password policy like alphanumeric), email(regular expressions),
		// phoneno(regular expressions)		
			//case 2 for login choice
		case 2:
			System.out.println("Enter username,password(8 char),email,phone no to signup");
			// TODO:reuse existing variables as much as possible
			username = sc.next();
			if (UserTable.checkUserUnique(username) == true)
				System.out.println("Enter other username");
			password = sc.next();
			if (PasswordValidation.passwordValidation(password))
				System.out.println("valid");
			else {
				System.out.println("invalid password");
				return;
			}

			String email = sc.next();
			if (EmailValidator.isValid(email))
				System.out.println("valid");
			else {
				System.out.println("invalid emailId");
				return;
			}

			Long phoneNo = sc.nextLong();
			String ph = String.valueOf(phoneNo);
			if (MobileNumberValidation.isValid(ph))
				System.out.println("valid");
			else {
				System.out.println("invalid phone no");
				return;
			}

			// System.out.println(User.checkUserUnique(username, email, phoneNo));
			// TODO:change the j from int to bool
			boolean conditionSignup = UserTable.signUp(username, password, email, phoneNo);
			// TODO:Fix indentation and spacings across the codebase

			String signUp = (conditionSignup != false ? "signUp successful" : "Try again");
			 System.out.println(signUp);			
			break;

		default:
			System.out.println("Sorry,wrong choice entered");
			return;
		}
		sc.close();
	}
}
