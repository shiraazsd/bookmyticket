package com.bookmytkt.application;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import com.bookmytkt.InterfaceDao.AddressDao;
import com.bookmytkt.InterfaceDao.DocIdDao;
import com.bookmytkt.InterfaceDao.PassengerDao;
import com.bookmytkt.InterfaceDao.ReservationDao;
import com.bookmytkt.InterfaceDao.StationDao;
import com.bookmytkt.InterfaceDao.TrainTableDao;
import com.bookmytkt.InterfaceDao.UserDao;
import com.bookmytkt.InterfaceDao.UserReservationDao;
import com.bookmytkt.Newdao.*;
import com.bookmytkt.utility.MobileNumberValidation;
import com.bookmytkt.utility.PasswordValidation;
import com.bookmytkt.utility.PnrNoGenerator;
import com.bookmytkt.utility.getCurrentTimeStamp;

import com.bookmytkt.dto.Station;

import com.bookmytkt.dto.Address;
import com.bookmytkt.dto.Passenger;
import com.bookmytkt.dto.Reservation;
import com.bookmytkt.dto.TrainRoute;
import com.bookmytkt.dto.Traintable;
import com.bookmytkt.dto.User;
import com.bookmytkt.dto.UserReservation;
import com.bookmytkt.utility.EmailValidator;
import com.bookmytkt.utility.IsValidDate;

public class JunWelcome {

	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException, ParseException {
		
		// This is a new comment
				// TODO:1)All the class names should start with Capital Letters 2)All the method
				// and variable names should follow camel case naming convention strictly
				// updateProfileDetails(), do not use numbers, special characters
				
				// use 'return' to come oout of a funct and not break
				//put functions into a class with meaningful name
				
				// use private fields in DAO pkg class
				//use get() and set() method
		//use final for constants
				

			

				Scanner sc = new Scanner(System.in);

				System.out.println("Enter 1 to Login,2 to Register");
				int ch = sc.nextInt();

				switch (ch) {

				case 1:
					System.out.println("Enter username and password");
					String username = sc.next();
					String password = sc.next();

					// TODO:
					// 1)Refactor the complete codebase to move all the switch case flow and input
					// calls in main class.
					// 2)The functions should perform there individual task and return the results
					// strictly
					// 3)Any console outputs should be printed in the main strictly
					boolean conditionLogin = UserDaoImpl.logIn(username, password);
					
					if (conditionLogin != false) {
						System.out.println("login successful");

						System.out.println("Enter 1 to viewTrain,  2 for view booking, 3 to change password");

						int viewTrainchoice = sc.nextInt();

						switch (viewTrainchoice) {

						case 1:

							System.out.println("Enter src station ,dest station ,journey date as per the data");
							StationDaoImpl da  = new StationDaoImpl();
					    	List<Object> l = da.getAll();
					    	for(Object sta:l) {
					    		Station st = (Station)sta;
					    		System.out.println(st.getId()+"\t"+st.getStationName());
					    	}
							
														
							int srcId = sc.nextInt();
							int destId = sc.nextInt();
						    String journeyDate = sc.next();
						    
						    boolean cond = IsValidDate.isValid(journeyDate);
							if(cond==true) {
								System.out.println("valid date");
							}
							else return;
						    
							System.out.println("TrainNo   Train name  trainId  noOfSeats   "
									+ "   arrivTime             DeptTime     arrivCity  "
									+ " DeptCity      TravelTime(hh:mm)");
							ArrayList<String> al = new ArrayList<String>();					
							
							
							al = TrainRouteDaoImpl.Display( srcId, destId);

							for (String obj : al) {
								System.out.print(obj+"  ");
								
							}

							System.out.println();

							System.out.println("Enter trainNO ,trainId ,no of tkt to be booked");
							int trainNo = sc.nextInt();
							int trainId = sc.nextInt();

							int noOfTkt = sc.nextInt();

							boolean seatAvailcondition = TrainTableDaoImpl.CheckSeatAvailability(trainNo, noOfTkt);
							if (seatAvailcondition == false) {
								System.out.println("Sorry,no more seats available");
								break;
							}

							//int trainId = 0;

							String pnrNo = PnrNoGenerator.PNRGen();

							for (int i = 0; i < noOfTkt; i++) {
								// trainId = BookTkt.BookTkt(pnrNo);

								System.out.println("Enter name,dob(dd-MM-yyyy),"
										+ " doc id "
										+ ", address id  per the table\n");

								System.out.println("docId    typeOfId\n");

								DocIdDao dao  = new DocIdDaoImpl();
						    	List<Object> list = dao.getAll();
						    	for(Object city:list) {
						    		
						    		System.out.println(city);						    		
						    	}
								//done
								System.out.println("\ncityid   addresscity\n");
								AddressDao addr =  new AddressDaoImpl();
								list = addr.getAll();
								for(Object city:list) {
									Address adr = (Address)city;
						    		System.out.println(adr.getId()+"\t"+adr.getAddress());
								}
								//done
								/*
								System.out.println("\nTrainId  name \n");
								TrainTableDao tr = new TrainTableDaoImpl();
								list = tr.getAll();
								for(Object obj:list) {
									
									Traintable t = (Traintable)obj;
						    		System.out.println(t.getId()+"\t"+t.getTrainName());
								}
								*/
								String name = sc.next();
								String dob = sc.next();
								//String journeyDate = sc.next();
								
								
								
								int docId = sc.nextInt();
								int cityId = sc.nextInt();
								//trainId = sc.nextInt();

								String status = "cnf";
								//todo addpassenger
								
								SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
								
								//journeyDate typecast to sql
								java.util.Date journDate = sdf1.parse(journeyDate);
								java.sql.Date sqljournDate = new java.sql.Date(journDate.getTime());
								
								//do typecast to sql
								java.util.Date datet = sdf1.parse(dob);
								java.sql.Date sqlDob = new java.sql.Date(datet.getTime());
								
								//int reservId = 0;
								Passenger c = null;
								c = new Passenger(name,sqlDob,docId,cityId);
								PassengerDao daoP  = new PassengerDaoImpl();
								int  passengId = daoP.add(c);								
								String s = (passengId != 0 ? "passeng add successful" : "Try again");
								System.out.println(s);
								System.out.println(passengId);
								
								
								ReservationDao daoR  = new ReservationDaoImpl();
								Reservation r = null;	
								java.util.Date today = new java.util.Date();
								java.sql.Date sqlDate = new java.sql.Date(today.getTime());
								
								List<String > lis = new ArrayList<>();
								lis = TrainRouteDaoImpl.getSrcAndDestSationName(srcId, destId);
									//for(String city:lis) {
						    		
						    		String srcstn = lis.get(0);
						    		String deststn = lis.get(1);

						    		
									//}
						    		
						    		String arrivTime = "3:50";
						    		String deptTime = "6:50";


						    	 r = new Reservation( passengId,trainId , sqlDate,sqljournDate,status,PnrNoGenerator.PNRGen(),srcstn,deststn,arrivTime,deptTime);
							     int reservId = daoR.add(r);
							     s = (reservId != 0  ? "reserv add successful" : "Try again");
								System.out.println(s);
								System.out.println(reservId);

								
								

								//BookTkt.addPassenger(name, dob, docId, cityId, trainId, journeyDate, status, pnrNo);

							}//end of for loop
							
							//todo
							
							
							
								boolean condn = TrainTableDaoImpl.updateSeats(noOfTkt, trainId);
								if (condn != false) {
									System.out.println("Tkt booked successfully");
									Float amt = BillingDaoImpl.billing(trainId, noOfTkt);
									System.out.println("Total fare is: "+amt);
									// trainTkt.updatetrainTkttable(trainNo, trainId, pnrNo);

								} else
									System.out.println("Sorry,No seats available");
							
							break;
							
							//case 2 for viewTrainChoice
						case 2:

							//System.out.println("Enter passId,reservId,PnrNo ");
							//int passengId = sc.nextInt();
							//int reservId = sc.nextInt();
							
							

							System.out.println("Enter 1 to cancel booking,  2 to print tkt , 3 to viewBookings");

							int bookingch = sc.nextInt();
							System.out.println("Enter PnrNo ");

							pnrNo = sc.next();

							switch (bookingch) {

							case 1:
								boolean b = false;
								ReservationDaoImpl dao  = new ReservationDaoImpl();
								b  = dao.deleteByPnr(pnrNo);
								String str = (b != false?"Deleted succesfully ":"Record not found");
					   			System.out.println(str);								
								break;

							case 2:
								
								//Do it

								//PrintTkt.printTkt(pnrNo);
								break;
								
							case 3:
								System.out.println("Enter userid");
								int userId = sc.nextInt();
								
								UserReservationDao userDao = new UserReservationDaoImpl();
								List<UserReservation> list = userDao.getUserByUserId(userId);
								for(Object obj:list)
									System.out.println(obj);							
								
								//Do it

								//PrintTkt.printTkt(pnrNo);
								break;
							}

							break;
							
							//case 3 for viewTrainChoice
						case 3:

							System.out.println("Enter userName,password to change");
							String userNameUpdate = sc.next();
							String passwordUpdate = sc.next();
							UserDaoImpl.changePassword(userNameUpdate, passwordUpdate);
							break;

						}

					} else //else block for if login
						System.out.println("invalid username or password");
					break;
				// TODO:Add validations for username(should be unique), password(use some
				// password policy like alphanumeric), email(regular expressions),
				// phoneno(regular expressions)		
					//case 2 for login choice
				case 2:
					System.out.println("Enter username,password(8 char),email,phone no to signup");
					// TODO:reuse existing variables as much as possible
					
					//make separate checkUnique functions for username,email,phone 
					username = sc.next();
					final String un = "username";//final used
					if (UserDaoImpl.checkUnique(un,username) != false) {
						System.out.println("invalid username");
					    return;  
					}
					password = sc.next();							
					
					if (PasswordValidation.passwordValidation(password))
						System.out.println("valid");
					else {
						System.out.println("invalid password");
						return;
					}
					
					
					String email = sc.next();
					final String em = "email";//final used
					if (UserDaoImpl.checkUnique("email",email) != false) {
						System.out.println("invalid Email");
						return;
					}

					
					if (EmailValidator.isValid(email))
						System.out.println("valid");
					else {
						System.out.println("invalid emailId");
						return;
					}

					Long phoneNo = sc.nextLong();
					final String phn = "phone";//final used
					String ph = String.valueOf(phoneNo);
					if (UserDaoImpl.checkUnique(phn,ph) != false) {
						System.out.println("invalid phone");
						return;
					}
					
					if (MobileNumberValidation.isValid(ph))
						System.out.println("valid");
					else {
						System.out.println("invalid phone no");
						return;
					}

					// System.out.println(User.checkUserUnique(username, email, phoneNo));
					// TODO:change the j from int to bool
					//boolean b = false;
					User c = null;
				  	UserDao dao  = new  UserDaoImpl();
				  	c = new User(username,"misbah","kn3","lko","mum",email, phoneNo.toString(),password);
				   int b = dao.add(c);
					//System.out.println(x);
					// TODO:Fix indentation and spacings across the codebase

					String signUp = (b != 0 ? "signUp successful" : "Try again");
					 System.out.println(signUp);			
					break;

				default:
					System.out.println("Sorry,wrong choice entered");
					return;
				}
				sc.close();

	}

}
