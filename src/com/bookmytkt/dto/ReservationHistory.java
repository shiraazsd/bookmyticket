package com.bookmytkt.dto;

public class ReservationHistory {
	private int id;
	private int userHistoryId;
	private int trainId;
	private String departureDate;
	private String departureTime;
	public int getUserHistoryId() {
		return userHistoryId;
	}
	
	public ReservationHistory() {
	
	}
	



	public ReservationHistory(int userHistoryId, int trainId, String departureDate, String departureTime) {
		
		this.userHistoryId = userHistoryId;
		this.trainId = trainId;
		this.departureDate = departureDate;
		this.departureTime = departureTime;
	}

	public ReservationHistory(int id, int userHistoryId, int trainId, String departureDate, String departureTime) {
		this.id = id;
		this.userHistoryId = userHistoryId;
		this.trainId = trainId;
		this.departureDate = departureDate;
		this.departureTime = departureTime;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUserHistoryId(int userHistoryId) {
		this.userHistoryId = userHistoryId;
	}
	public int getTrainId() {
		return trainId;
	}
	public void setTrainId(int trainId) {
		this.trainId = trainId;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	@Override
	public String toString() {
		return "reservationHistory [id=" + id + ", userHistoryId=" + userHistoryId + ", trainId=" + trainId
				+ ", departureDate=" + departureDate + ", departureTime=" + departureTime + "]";
	}
	
}
