package com.bookmytkt.dto;

public class Station {
	


	private int id;
	private String stationName;
	private int cityId;
	
	
	public Station() {
		
	}
	
	public Station(String stationName, int cityId) {
		
		this.stationName = stationName;
		this.cityId = cityId;
		
	}

	public Station(int id, String stationName, int cityId) {
	
		this.id = id;
		this.stationName = stationName;
		this.cityId = cityId;
		
	}
	public String getStationName() {
		return stationName;
	}
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
	public int getCityId() {
		return cityId;
	}
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	@Override
	public String toString() {
		return "Station [id=" + id + ", stationName=" + stationName + ", cityId=" + cityId + "]";
	}

}
