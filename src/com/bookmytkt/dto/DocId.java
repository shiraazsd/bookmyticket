package com.bookmytkt.dto;

public class DocId {
	private int id;
	private String typeOfId;
	
	public DocId() {
		
	}
	
	public DocId(String typeOfId) {
		this.typeOfId = typeOfId;
	}

	public DocId(int id, String typeOfId) {
		
		this.id = id;
		this.typeOfId = typeOfId;
	}
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTypeOfId() {
		return typeOfId;
	}
	public void setTypeOfId(String typeOfId) {
		this.typeOfId = typeOfId;
	}

	@Override
	public String toString() {
		return "  " + id + "      " + typeOfId ;
	}
	
}
