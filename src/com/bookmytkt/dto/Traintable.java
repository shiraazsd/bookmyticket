package com.bookmytkt.dto;

public class Traintable {
	private int id;
	private String trainName;
	private int trainNo;
	private float fare;
	private int noOfSeats;
	
	
	
	public Traintable() {
	
	}	

	public Traintable(String trainName, int trainNo, float fare, int noOfSeats) {
		this.trainName = trainName;
		this.trainNo = trainNo;
		this.fare = fare;
		this.noOfSeats = noOfSeats;
	}



	public Traintable(int id, String trainName, int trainNo, float fare, int noOfSeats) {
	
		this.id = id;
		this.trainName = trainName;
		this.trainNo = trainNo;
		this.fare = fare;
		this.noOfSeats = noOfSeats;
	}
	

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getTrainName() {
		return trainName;
	}

	public void setTrainName(String trainName) {
		this.trainName = trainName;
	}

	public int getTrainNo() {
		return trainNo;
	}

	public void setTrainNo(int trainNo) {
		this.trainNo = trainNo;
	}

	public float getFare() {
		return fare;
	}

	public void setFare(float fare) {
		this.fare = fare;
	}

	public int getNoOfSeats() {
		return noOfSeats;
	}

	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}



	@Override
	public String toString() {
		return "Traintable [id=" + id + ", trainName=" + trainName + ", trainNo=" + trainNo + ", fare=" + fare
				+ ", noOfSeats=" + noOfSeats + "]";
	}
	
}
