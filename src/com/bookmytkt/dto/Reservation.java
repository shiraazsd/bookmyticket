package com.bookmytkt.dto;

import java.util.Date;

public class Reservation {
	
	private int id;
	private int passengerId;
	private int trainId;
	private Date reservationDate;
	private Date journeyDate;
	private String statusOfReservation;
	private String pnrNo;
	private String srcStation;
	private String destStation;
	private String arrivalTime;
	private String departureTime;
	
	public Reservation() {
	
	}
	





	public Reservation(int id, int passengerId, int trainId, Date reservationDate, Date journeyDate,
			String statusOfReservation, String pnrNo, String srcStation, String destStation, String arrivalTime,
			String departureTime) {
		super();
		this.id = id;
		this.passengerId = passengerId;
		this.trainId = trainId;
		this.reservationDate = reservationDate;
		this.journeyDate = journeyDate;
		this.statusOfReservation = statusOfReservation;
		this.pnrNo = pnrNo;
		this.srcStation = srcStation;
		this.destStation = destStation;
		this.arrivalTime = arrivalTime;
		this.departureTime = departureTime;
	}






	public Reservation(int passengerId, int trainId, Date reservationDate, Date journeyDate, String statusOfReservation,
			String pnrNo, String srcStation, String destStation, String arrivalTime, String departureTime) {
		super();
		this.passengerId = passengerId;
		this.trainId = trainId;
		this.reservationDate = reservationDate;
		this.journeyDate = journeyDate;
		this.statusOfReservation = statusOfReservation;
		this.pnrNo = pnrNo;
		this.srcStation = srcStation;
		this.destStation = destStation;
		this.arrivalTime = arrivalTime;
		this.departureTime = departureTime;
	}
	
	






	public String getArrivalTime() {
		return arrivalTime;
	}






	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}






	public String getDepartureTime() {
		return departureTime;
	}






	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}






	public String getSrcStation() {
		return srcStation;
	}



	public void setSrcStation(String srcStation) {
		this.srcStation = srcStation;
	}



	public String getDestStation() {
		return destStation;
	}



	public void setDestStation(String destStation) {
		this.destStation = destStation;
	}



	public Reservation(int id ,int passengerId, int trainId, Date reservationDate, Date journeyDate,
			String statusOfReservation) {
		this.id = id;
	
		this.passengerId = passengerId;
		this.trainId = trainId;
		this.reservationDate = reservationDate;
		this.journeyDate = journeyDate;
		this.statusOfReservation = statusOfReservation;
	}



	public Reservation(int id, int passengerId, int trainId, Date reservationDate, Date journeyDate,
			String statusOfReservation, String pnrNo) {
		
		this.id = id;
		this.passengerId = passengerId;
		this.trainId = trainId;
		this.reservationDate = reservationDate;
		this.journeyDate = journeyDate;
		this.statusOfReservation = statusOfReservation;
		this.pnrNo = pnrNo;
	}
	
	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public int getPassengerId() {
		return passengerId;
	}
	public void setPassengerId(int passengerId) {
		this.passengerId = passengerId;
	}
	public int getTrainId() {
		return trainId;
	}
	public void setTrainId(int trainId) {
		this.trainId = trainId;
	}
	public Date getReservationDate() {
		return reservationDate;
	}
	public void setReservationDate(Date reservationDate) {
		this.reservationDate = reservationDate;
	}
	public Date getJourneyDate() {
		return journeyDate;
	}
	public void setJourneyDate(Date journeyDate) {
		this.journeyDate = journeyDate;
	}
	public String getStatusOfReservation() {
		return statusOfReservation;
	}
	public void setStatusOfReservation(String statusOfReservation) {
		this.statusOfReservation = statusOfReservation;
	}
	public String getPnrNo() {
		return pnrNo;
	}
	public void setPnrNo(String pnrNo) {
		this.pnrNo = pnrNo;
	}






	@Override
	public String toString() {
		return "Reservation [id=" + id + ", passengerId=" + passengerId + ", trainId=" + trainId + ", reservationDate="
				+ reservationDate + ", journeyDate=" + journeyDate + ", statusOfReservation=" + statusOfReservation
				+ ", pnrNo=" + pnrNo + ", srcStation=" + srcStation + ", destStation=" + destStation + ", arrivalTime="
				+ arrivalTime + ", departureTime=" + departureTime + "]";
	}




	
	
	
}
