package com.bookmytkt.dto;

public class userHistory {
	private int id;
	private String firstName;
	private String lastName;
	private String Address;
	private String city;
	private String emailId;
	private String phoneNo;
	private String password;
	
	public userHistory() {
	
	}
	
	public userHistory(String firstName, String lastName, String address, String city, String emailId, String phoneNo,
			String password) {
		
		this.firstName = firstName;
		this.lastName = lastName;
		Address = address;
		this.city = city;
		this.emailId = emailId;
		this.phoneNo = phoneNo;
		this.password = password;
	}

	public userHistory(int id, String firstName, String lastName, String address, String city, String emailId,
			String phoneNo, String password) {
		
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		Address = address;
		this.city = city;
		this.emailId = emailId;
		this.phoneNo = phoneNo;
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "userHistory [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", Address=" + Address
				+ ", city=" + city + ", emailId=" + emailId + ", phoneNo=" + phoneNo + ", password=" + password + "]";
	}
	
}
