package com.bookmytkt.dto;

public class Seats {
	private int id;
	private String dateOfBooking;
	private int trainNo;
	private int seatsBooked;
	
	public Seats() {
	
	}
	
	
	public Seats(String dateOfBooking, int trainNo, int seatsBooked) {
		
		this.dateOfBooking = dateOfBooking;
		this.trainNo = trainNo;
		this.seatsBooked = seatsBooked;
	}


	public Seats(int id, String dateOfBooking, int trainNo, int seatsBooked) {
		
		this.id = id;
		this.dateOfBooking = dateOfBooking;
		this.trainNo = trainNo;
		this.seatsBooked = seatsBooked;
	}
	
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getDateOfBooking() {
		return dateOfBooking;
	}
	public void setDateOfBooking(String dateOfBooking) {
		this.dateOfBooking = dateOfBooking;
	}
	public int getTrainNo() {
		return trainNo;
	}
	public void setTrainNo(int trainNo) {
		this.trainNo = trainNo;
	}
	public int getSeatsBooked() {
		return seatsBooked;
	}
	public void setSeatsBooked(int seatsBooked) {
		this.seatsBooked = seatsBooked;
	}


	@Override
	public String toString() {
		return "Seats [id=" + id + ", dateOfBooking=" + dateOfBooking + ", trainNo=" + trainNo + ", seatsBooked="
				+ seatsBooked + "]";
	}
	
}
