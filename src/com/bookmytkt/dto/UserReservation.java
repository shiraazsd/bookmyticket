package com.bookmytkt.dto;

public class UserReservation {
	
	private int id;
	private int userId;
	private int reservationId;
	
	
	public UserReservation() {
	}

	public UserReservation(int userId, int reservationId) {
		this.userId = userId;
		this.reservationId = reservationId;
	}
	
	public UserReservation(int id, int userId, int reservationId) {
		this.id = id;
		this.userId = userId;
		this.reservationId = reservationId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getReservationId() {
		return reservationId;
	}

	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}

	@Override
	public String toString() {
		return "UserReservation [id=" + id + ", userId=" + userId + ", reservationId=" + reservationId + "]";
	}
	
	
	

}
