package com.bookmytkt.dto;

public class TrainRoute {
	
	private int id;
	private int trainId;
	private int stationId;
	//private int destinationStation;
	private String arrivalTime;
	private String departureTime;
	
	
	public TrainRoute() {
	
	}
	
	public TrainRoute(int trainId, int stationId, String arrivalTime,
			String departureTime) {
	
		this.trainId = trainId;
		this.stationId = stationId;
		//this.destinationStation = destinationStation;
		this.arrivalTime = arrivalTime;
		this.departureTime = departureTime;
	}

	public TrainRoute(int id, int trainId, int stationId, String arrivalTime,
			String departureTime) {
		
		this.id = id;
		this.trainId = trainId;
		this.stationId = stationId;
		//this.destinationStation = destinationStation;
		this.arrivalTime = arrivalTime;
		this.departureTime = departureTime;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTrainId() {
		return trainId;
	}
	public void setTrainId(int trainId) {
		this.trainId = trainId;
	}
	public int getStationId() {
		return stationId;
	}
	public void setStationID(int stationId) {
		this.stationId = stationId;
	}
	
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	@Override
	public String toString() {
		return "TrainRoute [id=" + id + ", trainId=" + trainId + ", StationId=" + stationId
				+ ", arrivalTime=" + arrivalTime + ", departureTime="
				+ departureTime + "]";
	}
	
}
