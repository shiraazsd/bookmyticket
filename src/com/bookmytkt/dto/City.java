package com.bookmytkt.dto;

public class City {
	
	private int id;
	private String name;
	private String metroCity;
	
	public City() {
	}
	//constructor without id field
	public City(String name, String metroCity) {
		this.name = name;
		this.metroCity = metroCity;
	}
	//constructor with id field

	public City(int id, String name, String metroCity) {
	
		this.id = id;
		this.name = name;
		this.metroCity = metroCity;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMetroCity() {
		return metroCity;
	}
	public void setMetroCity(String metroCity) {
		this.metroCity = metroCity;
	}
	@Override
	public String toString() {
		return "City [id=" + id + ", name=" + name + ", metroCity=" + metroCity + "]";
	}
	
	
}
