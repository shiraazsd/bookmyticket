package com.bookmytkt.dto;

import java.util.Date;

public class Passenger {
	private int id;
	private String passengerName;
	private Date dob;
	private int docId;
	private int addressID;
	
	public Passenger() {
		
	}
	
	public Passenger(String passengerName, Date dob, int docId, int addressID) {
		this.passengerName = passengerName;
		this.dob = dob;
		this.docId = docId;
		this.addressID = addressID;
	}

	public Passenger(int id, String passengerName, Date dob, int docId, int addressID) {
	
		this.id = id;
		this.passengerName = passengerName;
		this.dob = dob;
		this.docId = docId;
		this.addressID = addressID;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassengerName() {
		return passengerName;
	}
	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}
	public Date getDOB() {
		return dob;
	}
	public void setDate(Date dob) {
		this.dob = dob;
	}
	public int getDocId() {
		return docId;
	}
	public void setDocId(int docId) {
		this.docId = docId;
	}
	public int getAddressID() {
		return addressID;
	}
	public void setAddressID(int addressID) {
		this.addressID = addressID;
	}

	@Override
	public String toString() {
		return "Passenger [id=" + id + ", passengerName=" + passengerName + ", Date=" + dob + ", docId=" + docId
				+ ", addressID=" + addressID + "]";
	}
	
}
