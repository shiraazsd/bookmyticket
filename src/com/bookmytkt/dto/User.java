package com.bookmytkt.dto;
//
public class User {
	private int id;
	private String username;
	private String firstName;
	private String lastName;
	private String address;
	private String city;
	private String emailId;
	private String phoneNo;
	private String password;
	
	
	public User() {
		
	}
	
	
	
	
	public User(String username, String firstName, String lastName, String address, String city, String emailId,
			String phoneNo, String password) {
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.city = city;
		this.emailId = emailId;
		this.phoneNo = phoneNo;
		this.password = password;
	}
	




	public User(int id, String username, String firstName, String lastName, String address, String city, String emailId,
			String phoneNo, String password) {
		this.id = id;
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.city = city;
		this.emailId = emailId;
		this.phoneNo = phoneNo;
		this.password = password;
	}




	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}




	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", address=" + address + ", city=" + city + ", emailId=" + emailId + ", phoneNo=" + phoneNo
				+ ", password=" + password + "]";
	}
}
