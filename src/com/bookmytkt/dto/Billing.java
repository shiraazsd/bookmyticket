package com.bookmytkt.dto;

public class Billing {
	
	private int id;
	private int reservationId;
	private float amount;
	//constructor with id field
	public Billing(int id, int reservationId, float amount) {
		
		this.id = id;
		this.reservationId = reservationId;
		this.amount = amount;
	}
	
	public Billing() {
		
	}
	//constructor without id field	
	public Billing(int reservationId, float amount) {
		this.reservationId = reservationId;
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "Billing [id=" + id + ", reservationId=" + reservationId + ", amount=" + amount + "]";
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getReservationId() {
		return reservationId;
	}
	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	
}
