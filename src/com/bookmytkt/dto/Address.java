package com.bookmytkt.dto;

import java.util.List;

public class Address {
		private int id;
		private String address;
		private int cityId;	
		
		
		public Address() {
			
		}
		//constructor with id field
		public Address(int id,String address,int cityId){
			this.id = id;
			this.address  = address;
			this.cityId = cityId;
		}
		
		//constructor without id field
		public Address(String address, int cityId) {
			this.address = address;
			this.cityId = cityId;
		}
		
		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public int getCityId() {
			return cityId;
		}
		public void setCityId(int cityId) {
			this.cityId = cityId;
		}
		@Override
		public String toString() {
			return "Address [id=" + id + ", address=" + address + ", cityId=" + cityId + "]";
		}		
		
}

