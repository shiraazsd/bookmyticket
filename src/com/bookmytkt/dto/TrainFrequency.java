package com.bookmytkt.dto;

public class TrainFrequency {
	private int id;
	private int trainId;
	private String frequencyDays;	
	
	public TrainFrequency() {
	
	}
	
	public TrainFrequency(int trainId, String frequencyDays) {
	
		this.trainId = trainId;
		this.frequencyDays = frequencyDays;
	}

	public TrainFrequency(int id, int trainId, String frequencyDays) {
	
		this.id = id;
		this.trainId = trainId;
		this.frequencyDays = frequencyDays;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTrainId() {
		return trainId;
	}
	public void setTrainId(int trainId) {
		this.trainId = trainId;
	}
	public String getFrequencyDays() {
		return frequencyDays;
	}
	public void setFrequencyDays(String frequencyDays) {
		this.frequencyDays = frequencyDays;
	}

	@Override
	public String toString() {
		return "TrainFrequency [id=" + id + ", trainId=" + trainId + ", FrequencyDays=" + frequencyDays + "]";
	}
	
}
