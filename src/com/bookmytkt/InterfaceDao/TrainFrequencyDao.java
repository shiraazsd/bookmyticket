package com.bookmytkt.InterfaceDao;

import java.sql.SQLException;
import java.util.List;

import com.bookmytkt.MainInterface.MainInterface;
import com.bookmytkt.dto.*;

public interface TrainFrequencyDao extends MainInterface{
	
	public TrainFrequency getTrainFreqById(int Id) throws SQLException;
	   

    public List<TrainFrequency> getTrainFreuencyByTrainId(int trainId);
    public List<TrainFrequency> getTrainFrequencyByDays(String frequencyDays);
   

}
