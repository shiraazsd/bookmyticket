package com.bookmytkt.InterfaceDao;

import java.sql.SQLException;
import java.util.List;

import com.bookmytkt.MainInterface.MainInterface;
import com.bookmytkt.dto.Billing;
import com.bookmytkt.dto.City;

public interface CityDao extends MainInterface{
	
	
	   
    public City getCityById(int cityId) throws SQLException;
    public City getCityByName(String name);
    public List<City> getCityByMetroCity(String metroCity);
   

}
