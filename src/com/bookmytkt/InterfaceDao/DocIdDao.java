package com.bookmytkt.InterfaceDao;

import java.sql.SQLException;
import java.util.List;

import javax.print.Doc;

import com.bookmytkt.MainInterface.MainInterface;
import com.bookmytkt.dto.City;
import com.bookmytkt.dto.DocId;

public interface DocIdDao extends MainInterface{
	
	
    public List<Object> getIdByType(String type);
    public DocId getdocById(int Id);

}
