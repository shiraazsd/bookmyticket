package com.bookmytkt.InterfaceDao;

import java.sql.SQLException;

import java.util.List;
import com.bookmytkt.*;
import com.bookmytkt.MainInterface.MainInterface;
import com.bookmytkt.dto.User;
import com.bookmytkt.dto.UserReservation;

public interface UserReservationDao extends MainInterface{
	
	
	public UserReservation getUserReservationById(int id) throws SQLException;
	public UserReservation getUserByReservationId(int reservationId) throws SQLException;

    public List<UserReservation> getUserByUserId(int userId);

}
