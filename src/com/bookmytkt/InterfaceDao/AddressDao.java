package com.bookmytkt.InterfaceDao;
import com.bookmytkt.dto.*;

import java.util.List;

import com.bookmytkt.MainInterface.MainInterface;

public interface AddressDao extends MainInterface{	    
	    
	    public Address getAddress(int addressId);
	    
	    public List<Address> getAddressByaddress(String address);
	    public List<Address> getAddressByCityId(int cityId);  

}
