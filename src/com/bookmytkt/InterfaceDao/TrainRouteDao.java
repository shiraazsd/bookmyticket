package com.bookmytkt.InterfaceDao;

import java.sql.SQLException;
import java.util.List;

import com.bookmytkt.MainInterface.MainInterface;
import com.bookmytkt.dto.TrainFrequency;
import com.bookmytkt.dto.TrainRoute;

public interface TrainRouteDao extends MainInterface{
	
	public TrainRoute getTrainRouteById(int id) throws SQLException;	   

    public List<TrainRoute> getTrainRouteByTrainId(int trainId);
    public List<TrainRoute> getTrainRouteByStationId(int StaionId);
    //public List<TrainRoute> getTrainRouteByDestinationStation(int destinatonStaion);
    public List<TrainRoute> getTrainRouteByArrivalTime(String arrivalTime);
    public List<TrainRoute> getTrainRouteByDepartureTime(String departureTime);   

}
