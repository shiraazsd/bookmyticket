package com.bookmytkt.InterfaceDao;

import java.sql.SQLException;
import java.util.List;

import com.bookmytkt.MainInterface.MainInterface;
import com.bookmytkt.dto.TrainRoute;
import com.bookmytkt.dto.Traintable;

public interface TrainTableDao extends MainInterface{
	
	public Traintable getTrainTableById(int id) throws SQLException;	   

    public List<Traintable> getTrainTableByTrainName(String trainName);
    public List<Traintable> getTrainTableByTrainNo(int trainNo);
    public List<Traintable> getTrainTableByTrainFare(Float trainFare);
    public List<Traintable> getTrainTableByNoOfSeats(int noOfSeats);
}
