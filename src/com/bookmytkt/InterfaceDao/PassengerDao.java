package com.bookmytkt.InterfaceDao;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.bookmytkt.MainInterface.MainInterface;
import com.bookmytkt.dto.City;
import com.bookmytkt.dto.Passenger;

public interface PassengerDao extends MainInterface {
	
	
	   
    public Passenger getPassengerById(int cityId) throws SQLException;
  
    public List<Passenger> getPassengerByPassengerName(String passengerName);
    public List<Passenger> getPassengerByDOB(Date dob);
    public List<Passenger> getPassengerByDocId(int docId);
    public List<Passenger> getPassengerByAddressId(int AddressId);
  

}
