package com.bookmytkt.InterfaceDao;

import java.sql.SQLException;
import java.util.List;

import com.bookmytkt.MainInterface.MainInterface;
import com.bookmytkt.dto.*;
import com.bookmytkt.dto.ReservationHistory;

public interface SeatsDao extends MainInterface{
	
	public Seats getSeatsById(int Id) throws SQLException;
   

    public List<Seats> getSeatsByDateOfBooking(String dateOfBooking);
    public List<Seats> getSeatsByTrainNo(int trainNo);
    public List<Seats> getSeatsBySeatsBooked(int seatsBooked);

}
