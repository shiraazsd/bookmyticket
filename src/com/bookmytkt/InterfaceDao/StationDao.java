package com.bookmytkt.InterfaceDao;

import java.sql.SQLException;
import java.util.List;

import com.bookmytkt.MainInterface.MainInterface;
import com.bookmytkt.dto.Billing;
import com.bookmytkt.dto.Station;

public interface StationDao extends MainInterface {
	
	
	   
    public Station getstationById(int stationId) throws SQLException;
    public Station getStationByCityId(int CityId);
    public Station getStationByStationName(String stationName);
  

}
