package com.bookmytkt.InterfaceDao;

import java.sql.SQLException;
import java.util.List;

import com.bookmytkt.MainInterface.MainInterface;
import com.bookmytkt.dto.Traintable;
import com.bookmytkt.dto.User;

public interface UserDao extends MainInterface{
	
	public User getUserById(int id) throws SQLException;
	public User getUserByUserName(String userName) throws SQLException;

    public List<User> getUserByFirstName(String firstName);
    public List<User> getUserByLastName(String lastName);
    public List<User> getUserByAddress(String address);

    public List<User> getUserByCity(String city);
    public List<User> getUserByPhoneNo(String phoneNo);
    public List<User> getUserByEmail(String email);
    public List<User> getUserByPassword(String password);
   

}
