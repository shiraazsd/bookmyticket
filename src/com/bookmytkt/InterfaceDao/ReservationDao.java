package com.bookmytkt.InterfaceDao;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.bookmytkt.MainInterface.MainInterface;
import com.bookmytkt.dto.Passenger;
import com.bookmytkt.dto.Reservation;

public interface ReservationDao extends MainInterface {
	
    public Reservation getReservationById(int Id) throws SQLException;
    public Reservation getReservationByPnrNo(String pnrNo);
    public List<Reservation> getReservationByPassengerId(int passengerId);
    public List<Reservation> getReservationByTrainId(int trainId);
    public List<Reservation> getReservationByReservationDate(Date reservationDate);
    public List<Reservation> getReservationByJourneyDate(Date journeyDate);
    public List<Reservation> getReservationByStatusOfReservation(String status);

}
