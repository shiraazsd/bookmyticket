package com.bookmytkt.InterfaceDao;
import java.sql.SQLException;
import java.util.List;

import com.bookmytkt.MainInterface.MainInterface;
import com.bookmytkt.dto.Billing;

public interface BillingDao extends MainInterface {
	
	  
	   
	    public Billing getBillingById(int billingId) throws SQLException;
	    
	    public List<Billing> getBillingByReservID(int ReservId);
	    public List<Billing> getBillingByAmt(Float Amt);
	   
	}


