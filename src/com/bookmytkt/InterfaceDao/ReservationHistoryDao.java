package com.bookmytkt.InterfaceDao;

import java.sql.SQLException;
import java.util.List;

import com.bookmytkt.MainInterface.MainInterface;
import com.bookmytkt.dto.ReservationHistory;;

//toDo
public interface ReservationHistoryDao  extends MainInterface{
	
	public ReservationHistory getReservationById(int Id) throws SQLException;
    public ReservationHistory getReservationByPnrNo(String pnrNo);
    public List<ReservationHistory> getReservationByPassengerId(int passengerId);
    public List<ReservationHistory> getReservationByTrainId(int trainId);
    public List<ReservationHistory> getReservationByReservationDate(String reservationDate);
    public List<ReservationHistory> getReservationByJourneyDate(String journeyDate);
    public List<ReservationHistory> getReservationByStatusOfReservation(String status);

}
