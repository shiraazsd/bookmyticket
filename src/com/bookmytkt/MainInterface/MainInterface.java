package com.bookmytkt.MainInterface;
import java.util.List;

import com.bookmytkt.dto.Billing;

public interface MainInterface {	
	
	public int add(Object obj);
	public boolean delete(int id);
	public boolean update(Object obj);
	public List<Object> getAll();
	public int findNextSeqVal();
}
