package com.bookmytkt.RowMapper;
import com.bookmytkt.dto.*;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BillingRowMapper implements RowMapper {

	@Override
	public Object mapRow(ResultSet rs) throws SQLException {
		Billing bil = new Billing();
		bil.setId(rs.getInt("id"));
		bil.setReservationId(rs.getInt("rserv_id"));
		bil.setAmount(rs.getFloat("amt"));
		return bil;
	}
}
