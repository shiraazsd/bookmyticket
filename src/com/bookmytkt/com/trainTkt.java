package com.bookmytkt.com;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class trainTkt {

	static int srcid;
	static int destid;

	trainTkt(int srcid, int destid) {
		this.srcid = srcid;
		this.destid = destid;

	}

	static void updatetrainTkttable(int trainNo, int trainId, String pnrNo) throws SQLException {

		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");

		// con = JDBCUtil.getOracleConnection();
		// PreparedStatement ps1=con.prepareStatement("insert into tickettable
		// values(tickettableseq.nextval,?,?,?,?,?,?,?,?)");
		// ResultSet rs1 = ps1.executeQuery();

		String qry = "select trainname ,passengername,train_no,journeydate,status,pnr_no  from passeng_res y inner join reserv_res x   on x.passeng_id = y.id inner join traintable_res z on z.id=x.train_id   where Pnr_no=?";
		PreparedStatement ps = con.prepareStatement(qry);
		ps.setString(1, pnrNo);

		ResultSet rs = ps.executeQuery();

		String output = "";
		// System.out.println("trainName nameOfPAsseng Trainid joourneyDate Status
		// PnrNo");
		// TODO:Use arraylist everywhere
		while (rs.next()) {

			// int trainid = rs.getInt("train_id");

			String trainnname = rs.getString("trainname");
			String passengername = rs.getString("passengername");
			int trainno = rs.getInt("train_no");
			Date journeydate = rs.getDate("journeydate");
			String status = rs.getString("status");

			PreparedStatement ps1 = con
					.prepareStatement("insert into tickettable values(tickettableseq.nextval,?,?,?,?,?,?,?,?)");

			ps1.setString(1, trainnname);
			ps1.setString(2, passengername);
			ps1.setInt(3, trainNo);
			ps1.setDate(4, (java.sql.Date) journeydate);
			ps1.setString(5, status);
			ps1.setString(6, pnrNo);

			String qry1 = "select q1.stationname ,q2.stationname  from (select stationname from station_res where id=?)q1, (select stationname from station_res where id=?)q2";
			PreparedStatement ps3 = con.prepareStatement(qry1);
			ps3.setInt(1, trainTkt.srcid);
			ps3.setInt(2, trainTkt.destid);

			ResultSet rs3 = ps3.executeQuery();
			while (rs3.next()) {
				String srcstnName = rs3.getString(1);
				String deststnName = rs3.getString(2);
				ps1.setString(7, srcstnName);
				ps1.setString(8, deststnName);

			}
			ps1.executeUpdate();

		}
	}
}
