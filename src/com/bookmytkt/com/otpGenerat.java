package com.bookmytkt.com;
import java.util.Random;

public class otpGenerat {
	static char[] OTP1(int len) {

		String numbers;

		if (len == 3)
			numbers = "123456789";
		else
			numbers = "0123456789";

		// Using random method
		Random rndm_method = new Random();

		char[] otp = new char[len];

		for (int i = 0; i < len; i++) {
			// Use of charAt() method : to get character value
			// Use of nextInt() as it is scanning the value as int
			otp[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
		}
		return otp;
	}

}
