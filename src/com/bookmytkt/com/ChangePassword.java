package com.bookmytkt.com;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ChangePassword {

	public static void changePassword(String userName, String password) throws SQLException {

		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");

		// con = JDBCUtil.getOracleConnection();
		PreparedStatement ps = con.prepareStatement("update users set password=? where firstname=?");
		ps.setString(1, password);
		ps.setString(2, userName);

		ps.executeUpdate();
	}
}
