package com.bookmytkt.com;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

public class DisplayTrainInfo {

	public static ArrayList<String> Display(int srcId,int DestId) throws SQLException, ClassNotFoundException, NumberFormatException, IOException {

		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");

		ArrayList<String> viewTrain = new ArrayList<String>();
		// Class.forName("oracle.jdbc.driver.OracleDriver");
		// Connection
		// con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","hello786");
		// con = JDBCUtil.getOracleConnection();
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(
				"select train_no,    trainname ,noofseats,arrivtime,depttime"
				+ " from TrainTable_Res x inner join TrainRoute_res y "
				+ "on x.id = y.trainid  where SRCSTATIONID = "
						+ srcId + " and DESTSTATIONID =" + DestId);
		ArrayList<String> al = new ArrayList<>();
		Date d1 = null;
		Date d2 = null;
		while (rs.next()) {			
			String trainNo = String.valueOf(rs.getInt("train_no"));
			String noOfSeats = String.valueOf(rs.getInt("noofseats"));
			String arrivalTime = String.valueOf(rs.getTime("arrivtime"));
			String departureTime = String.valueOf(rs.getTime("depttime"));
			al.add(trainNo);
            al.add(rs.getString("trainname"));
			al.add(noOfSeats);
			al.add(arrivalTime);
			al.add(departureTime);
		}
		Statement stmt1 = con.createStatement();
		ResultSet rs1 = stmt1
				.executeQuery("select stationname from Station_Res "
						+ "where ID = " + srcId + "or id=" + DestId);
		while (rs1.next()) {
			al.add(rs1.getString("stationname"));
		}
		Date depttime = null;
		Date arrivtime = null;
		Statement stmt3 = con.createStatement();
		ResultSet rs3 = stmt3.executeQuery(" select depttime,arrivtime   from Trainroute_Res where SRCSTATIONID ="
				+ srcId + " and DESTSTATIONID	 =" + DestId);
		while (rs3.next()) {
			depttime = rs3.getTime("depttime");
			arrivtime = rs3.getTime("arrivtime");			
			long diff = depttime.getTime() - arrivtime.getTime();
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffMinutes = diff / (60 * 1000) % 60;
			String timeInHour = String.valueOf(diffHours);
			String timeInMin = String.valueOf(diffMinutes);
			al.add(timeInHour);
			al.add(timeInMin);
		}
		return al;
	}

	public static boolean CheckSeatAvailability(int TrainNo, int NoOfTkt) throws ClassNotFoundException, SQLException {

		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");

		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(
				"select train_no,    trainname ,noofseats,arrivtime,depttime "
				+ "from TrainTable_Res x inner join TrainRoute_res y "
				+ "on x.id = y.trainid  where train_no ="
						+ TrainNo);
		while (rs.next()) {

			// output = output +" "+ rs.getInt(1) +" "+ rs.getString(2) +" "+rs.getInt(3)+"
			// "+ rs.getTime(4)+" "+rs.getTime(5)+" ";
			// TODO:use column names instead of column positions everywhere
			if (NoOfTkt > rs.getInt("noofseats")) {				
				return false;
			}
		}
		return true;
	}
}
