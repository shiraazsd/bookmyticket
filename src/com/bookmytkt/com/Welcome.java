package com.bookmytkt.com;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Welcome {

	public static void main(String[] args) throws SQLException, IOException, ClassNotFoundException, ParseException {
		// This is a new comment
		// TODO:1)All the class names should start with Capital Letters 2)All the method
		// and variable names should follow camel case naming convention strictly
		// updateProfileDetails(), do not use numbers, special characters
		
		// use 'return' to come oout of a funct and not break
		//put functions into a class with meaningful name
		
		// use private fields in DAO pkg class

		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter 1 to Login,2 to Register");
		int ch = sc.nextInt();

		switch (ch) {

		case 1:
			System.out.println("Enter username and password");
			String username = sc.next();
			String password = sc.next();

			// TODO:
			// 1)Refactor the complete codebase to move all the switch case flow and input
			// calls in main class.
			// 2)The functions should perform there individual task and return the results
			// strictly
			// 3)Any console outputs should be printed in the main strictly
			boolean conditionLogin = LogIn.logIn(username, password);
			if (conditionLogin != false) {
				System.out.println("login successful");

				System.out.println("Enter 1 to viewTrain,  2 for view booking, 3 to change password");

				int viewTrainchoice = sc.nextInt();

				switch (viewTrainchoice) {

				case 1:

					System.out.println("Enter src station ,dest station ,journey date as per the data");
					ArrayList<String> stationdata = Station.stationTable();

					for (String obj : stationdata) {
						System.out.print(obj);
					}

					int srcId = sc.nextInt();
					int destId = sc.nextInt();
					// String journeyDate = br.readLine();

					String output = "";

					Statement stmtTrainInfo = con.createStatement();
					ResultSet rsTrainInfo = stmtTrainInfo.executeQuery(
							"select train_no,    trainname ,noofseats,arrivtime,depttime from TrainTable_Res x inner join TrainRoute_res y on x.id = y.trainid  where SRCSTATIONID = "
									+ srcId + " and DESTSTATIONID =" + destId);

					System.out.println(
							"TrainNo   ,  Train name    noOfSeats      arrivTime             DeptTime     arrivCity   DeptCity      TravelTime");
					ArrayList<String> viewTrain = new ArrayList<String>();

					while (rsTrainInfo.next()) {

						/*
						 * output = output + " \n " + rsTrainInfo.getInt("train_no") + "       " +
						 * rsTrainInfo.getString("trainname") + "            " +
						 * rsTrainInfo.getInt("noofseats") + "         " +
						 * rsTrainInfo.getTime("arrivtime") + "             " +
						 * rsTrainInfo.getTime("depttime") + " ";
						 */
						String trainNo = String.valueOf(rsTrainInfo.getInt("train_no"));
						String noOfSeats = String.valueOf(rsTrainInfo.getInt("noofseats"));
						String arrivalTime = String.valueOf(rsTrainInfo.getTime("arrivtime"));
						String departureTime = String.valueOf(rsTrainInfo.getTime("depttime"));
						viewTrain.add(trainNo);
						viewTrain.add(rsTrainInfo.getString("trainname"));
						viewTrain.add(noOfSeats);
						viewTrain.add("     " + arrivalTime);
						viewTrain.add("         " + departureTime);

					}

					System.out.print(output);

					String output1 = "";

					Statement stmtStationName = con.createStatement();
					ResultSet rsStationName = stmtStationName.executeQuery(
							"select stationname from " + "Station_Res where ID = " + srcId + "or id=" + destId);
					while (rsStationName.next()) {

						output1 = output1 + "       " + rsStationName.getString("stationname") + "\t";
						viewTrain.add(rsStationName.getString("stationname"));

					}
					// String date = br.readLine();
					// System.out.print(output1);

					Date d = null;
					Date d3 = null;

					Statement stmtTime = con.createStatement();
					ResultSet rsTime = stmtTime.executeQuery(" select depttime,arrivtime   from Trainroute_Res "
							+ "where SRCSTATIONID =" + srcId + " and DESTSTATIONID	 =" + destId);
					while (rsTime.next()) {

						// use calendr

						d = rsTime.getTime("depttime");
						d3 = rsTime.getTime("arrivtime");
						long diff = d.getTime() - d3.getTime();
						long diffHours = diff / (60 * 60 * 1000) % 24;
						long diffMinutes = diff / (60 * 1000) % 60;
						// System.out.println(" " + diffHours + " hour " + diffMinutes + " min");

						String diffHour = String.valueOf("       " + diffHours + "  hour");
						String diffMin = String.valueOf(diffMinutes + "    min");
						viewTrain.add(diffHour);
						viewTrain.add(diffMin);

					}

					for (String obj : viewTrain) {
						System.out.print(obj + "       ");
					}

					System.out.println();

					System.out.println("Enter trainNO ,no of tkt to be booked");
					int trainNo = sc.nextInt();
					int noOfTkt = sc.nextInt();

					boolean seatAvailcondition = DisplayTrainInfo.CheckSeatAvailability(trainNo, noOfTkt);
					if (seatAvailcondition == false) {
						System.out.println("Sorry,no more seats available");
						break;
					}

					int trainId;

					String pnrNo = PnrNoGenerator.PNRGen();

					for (int i = 0; i < noOfTkt; i++) {
						// trainId = BookTkt.BookTkt(pnrNo);

						System.out.println("Enter name,dob(dd-MM-yyyy),"
								+ "journey date(dd-MM-yyyy), doc id as per table, address id as per table ,train id as per table\n");

						System.out.println("docId    typeOfId\n");

						ArrayList<String> docsId = new ArrayList<String>();
						Statement stmtDocId = con.createStatement();
						ResultSet rsDocId = stmtDocId.executeQuery(" select id,type from docid_Res");
						while (rsDocId.next()) {

							int docID = rsDocId.getInt("id");
							String type = rsDocId.getString("type");

							String outputt = "  " + docID + "     " + type;

							//String docId = String.valueOf(docID);
							docsId.add(outputt);
							// bookingData.add(type);

						}

						for (String obj : docsId) {
							System.out.println(obj);
						}

						System.out.println("\ncityid   city\n");

						ArrayList<String> cityyId = new ArrayList<String>();
						Statement stmtCityId = con.createStatement();
						ResultSet rsCityId = stmtCityId.executeQuery(" select id ,address from address_Res");
						while (rsCityId.next()) {

							int cityId = rsCityId.getInt(1);
							String addressID = rsCityId.getString(2);

							String outputt = " " + cityId + "     " + addressID;

							//String cityid = String.valueOf(cityId);
							cityyId.add(outputt);
							// bookingData.add(addressID);
						}

						for (String obj : cityyId) {
							System.out.println(obj);
						}

						System.out.println("\nTrainId  name \n");

						ArrayList<String> trainTable = new ArrayList<String>();
						Statement stmtTrainId = con.createStatement();
						ResultSet rsTrainId = stmtTrainId.executeQuery(" select id ,trainname from traintable_Res");
						while (rsTrainId.next()) {

							int trainidd = rsTrainId.getInt("id");
							String trainName = rsTrainId.getString("trainname");

							String outputt = " " + trainidd + "     " + trainName;
						//	String trainIdd = String.valueOf(trainidd);

							trainTable.add(outputt);
							// bookingData.add(trainName);

						}

						for (String obj : trainTable) {
							System.out.println(obj + "    ");
						}

						String name = sc.next();

						String dob = sc.next();
						SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
						java.util.Date date2 = sdf1.parse(dob);
						java.sql.Date sqlDob = new java.sql.Date(date2.getTime());

						String journeyDate = sc.next();
						//Date dateFormat = new SimpleDateFormat("dd-MM-yyyy").parse(journeyDate);
						java.util.Date journDate = sdf1.parse(journeyDate);
						java.sql.Date sqljournDate = new java.sql.Date(journDate.getTime());

						Calendar calendarDate = Calendar.getInstance();
						calendarDate.add(Calendar.DATE, 0);

						Calendar calendarMonth = Calendar.getInstance();
						calendarMonth.add(Calendar.MONTH, 2);

						if (sqljournDate.after(calendarDate.getTime())
								&& sqljournDate.before(calendarMonth.getTime())) {
							System.out.println("valid date");
						} else {
							System.out.println("invalid date");
							return;
						}

						int docId = sc.nextInt();

						int cityId = sc.nextInt();

						trainId = sc.nextInt();

						String status = "cnf";

						BookTkt.addPassenger(name, sqlDob, docId, cityId, trainId, sqljournDate, status, pnrNo);

					}

					Statement stmtTrainInf = con.createStatement();
					ResultSet rsTrainInf = stmtTrainInf
							.executeQuery("select id from TRAINTABLE_RES where train_no=" + trainNo);

					while (rsTrainInf.next()) {
						System.out.println("Tkt ");

						trainId = rsTrainInf.getInt("id");

						Billing.billing(trainId, noOfTkt);

						boolean cond = UpdateSeats.updateSeats(noOfTkt, trainId);
						if (cond != false) {
							System.out.println("Tkt booked successfully");
							// trainTkt.updatetrainTkttable(trainNo, trainId, pnrNo);

						} else
							System.out.println("Sorry,No seats available");
					}
					break;
					
					//case 2 for viewTrainChoice
				case 2:

					System.out.println("Enter passId,reservId,PnrNo ");
					int passengId = sc.nextInt();
					int reservId = sc.nextInt();

					pnrNo = sc.next();

					System.out.println("Enter 1 to cancel booking,  2 to print tkt");

					int bookingch = sc.nextInt();

					switch (bookingch) {

					case 1:

						CancelTkt.Delete(passengId, reservId);
						break;

					case 2:

						PrintTkt.printTkt(pnrNo);
						break;
					}

					break;
					
					//case 3 for viewTrainChoice
				case 3:

					System.out.println("Enter userName,password to change");
					String userNameUpdate = sc.next();
					String passwordUpdate = sc.next();
					ChangePassword.changePassword(userNameUpdate, passwordUpdate);
					break;

				}

			} else //else block for if login
				System.out.println("invalid username or password");

			break;

		// TODO:Add validations for username(should be unique), password(use some
		// password policy like alphanumeric), email(regular expressions),
		// phoneno(regular expressions)
		
			//case 2 for login choice
		case 2:
			System.out.println("Enter username,password(8 char),email,phone no to signup");
			// TODO:reuse existing variables as much as possible
			username = sc.next();
			if (UserTable.checkUserUnique(username) == true)
				System.out.println("Enter other username");
			password = sc.next();
			if (PasswordValidation.passwordValidation(password))
				System.out.println("valid");
			else {
				System.out.println("invalid password");
				break;
			}

			String email = sc.next();
			if (EmailValidator.isValid(email))
				System.out.println("valid");
			else {
				System.out.println("invalid emailId");
				break;
			}

			Long phoneNo = sc.nextLong();
			String ph = String.valueOf(phoneNo);
			if (MobileNumberValidation.isValid(ph))
				System.out.println("valid");
			else {
				System.out.println("invalid phone no");
				break;
			}

			// System.out.println(User.checkUserUnique(username, email, phoneNo));
			// TODO:change the j from int to bool
			boolean conditionSignup = UserTable.signUp(username, password, email, phoneNo);
			// TODO:Fix indentation and spacings across the codebase

			String signUp = (conditionSignup != false ? "signUp successful" : "Try again");
			 System.out.println(signUp);
			// if (conditionSignup != false {
			// System.out.println("signUp successful");
			// } else
			// System.out.println("Try again");
			break;

		default:
			System.out.println("Sorry,wrong choice entered");

		}
		sc.close();
	}
}
