package com.bookmytkt.com;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import JDBCUtil.JDBCUtil;

public class UserTable {

	public static boolean signUp(String name, String pwd, String email, Long phoneNo) throws SQLException {

		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "password");
		// Connection con = null;
		// ResultSet rs3 = null;
		PreparedStatement ps = null;
		// con = JDBCUtil.getOracleConnection();

		ps = con.prepareStatement("insert into usertable values(userseq.nextval,?,?,?,?)");

		// ps.setInt(1,id);
		ps.setString(1, name);
		ps.setString(2, pwd);
		ps.setString(3, email);
		ps.setLong(4, phoneNo);

		ps.executeUpdate();

		// JDBCUtil.cleanup(ps, con);
		return true;
	}
	
	
	public static boolean checkUserUnique(String val) throws SQLException {

		Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "hello786");
		// Connection con = null;
		// ResultSet rs3 = null;
		PreparedStatement ps = null;
		// con = JDBCUtil.getOracleConnection();
		
		String qry = "select username  from usertable where firstname=? ";

		ps = con.prepareStatement(qry);
		ps.setString(1, val);
				ResultSet rs = ps.executeQuery();
		
		while(rs.next()) {
			
			
			return true;
		}
		
		String qry1 = "select email  from usertable where email=? ";

		PreparedStatement ps1 = con.prepareStatement(qry1);
		
		ps1.setString(1, val);
		
        ResultSet rs1 = ps1.executeQuery();
		
		while(rs1.next()) {
			
			
			return true;
		}

		String qry2 = "select phoneno  from usertable where phoneno=? ";
		
		long phoneNo = Long.parseLong(val); 
				

		PreparedStatement ps2 = con.prepareStatement(qry2);
        ps2.setLong(1, phoneNo);
        ResultSet rs2 = ps2.executeQuery();
		
		while(rs2.next()) {
			
			
			return true;
		}


		// JDBCUtil.cleanup(ps, con);
		return false;
	}
	//static boolean checkUserUnique(String val) throws SQLException {}
	
	
	
}
