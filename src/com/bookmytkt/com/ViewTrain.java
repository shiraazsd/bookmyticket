package com.bookmytkt.com;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Scanner;

import javax.swing.text.View;

public class ViewTrain {

	static void ViewTrainInform()
			throws SQLException, IOException, NumberFormatException, ClassNotFoundException, ParseException {

		System.out.println("Enter choice");
		// TODO:Use scanner only. Remove bufferedreader completely
		// Remove extra spaces
		Scanner ch = new Scanner(System.in);

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter 1 to viewTrain,  2 for view booking, 3 to change password");

		int ch1 = ch.nextInt();

		switch (ch1) {

		case 1: {

			//DisplayTrainInfo.Display();
			System.out.println("Enter trainNO ,no of tkt to be booked");
			int trainNo = Integer.parseInt(br.readLine());
			int NoOfTkt = Integer.parseInt(br.readLine());

			boolean condition = DisplayTrainInfo.CheckSeatAvailability(trainNo, NoOfTkt);
			if (condition == false)
				break;

			int trainId = 0;

			String pnrNo = PnrNoGenerator.PNRGen();

			for (int i = 0; i < NoOfTkt; i++) {
				trainId = BookTkt.BookTkt(pnrNo);
			}

			Billing.billing(trainId, NoOfTkt);

			boolean cond = UpdateSeats.updateSeats(NoOfTkt, trainId);
			if (cond != false) {
				System.out.println("Tkt booked successfully");
				trainTkt.updatetrainTkttable(trainNo, trainId, pnrNo);

			} else
				System.out.println("Sorry,No seats available");

			break;
		}

		case 2: {

			System.out.println("Enter passId,reservId,PnrNo ");
			int passengId = Integer.parseInt(br.readLine());
			int ReservId = Integer.parseInt(br.readLine());

			String PnrNo = br.readLine();

			ViewBooking.viewBooking(passengId, ReservId, PnrNo);

			break;
		}

		case 3: {

			System.out.println("Enter userName,password to change");
			String userName = br.readLine();
			String password = br.readLine();
			ChangePassword.changePassword(userName, password);
			break;
		}
		}

	}
}
