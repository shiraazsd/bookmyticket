package com.bookmytkt.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class IsValidDate {	
	public static boolean isValid(String journeyDate) throws ParseException {		
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
		java.util.Date journDate = sdf1.parse(journeyDate);
		java.sql.Date sqljournDate = new java.sql.Date(journDate.getTime());
		Calendar calendarDate = Calendar.getInstance();
		calendarDate.add(Calendar.DATE, 0);
		Calendar calendarMonth = Calendar.getInstance();
		calendarMonth.add(Calendar.MONTH, 2);
		if (sqljournDate.after(calendarDate.getTime())
				&& sqljournDate.before(calendarMonth.getTime())) {
			return true;
		} else {
			System.out.println("invalid date");
			return false;
		}		
	}
}
