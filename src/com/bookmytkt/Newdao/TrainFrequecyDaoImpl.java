package com.bookmytkt.Newdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bookmytkt.InterfaceDao.TrainFrequencyDao;
import com.bookmytkt.dto.Seats;
import com.bookmytkt.dto.TrainFrequency;

import JDBCUtil.JDBCUtil;

public class TrainFrequecyDaoImpl implements TrainFrequencyDao {
	
	private static final String FINDSEQNEXTVAL = "SELECT trainfreqseq.nextval from dual";

	
	private static final String DELETE = "delete from trainfrequency  where id=?";
	private static final String FIND_ALL = "SELECT * FROM trainfrequency ORDER BY id";
	private static final String FIND_BY_ID = "SELECT * FROM trainfrequency WHERE id=?";

	private static final String FIND_BY_TRAINID = "SELECT * FROM trainfrequency WHERE trainid=?";
	private static final String FIND_BY_FREQUENCYDAYS = "SELECT * FROM trainfrequency WHERE frequencydays=?";

	
	private static final String INSERT = "INSERT INTO trainfrequency VALUES(?, ?,?)";
	private static final String UPDATE = "UPDATE trainfrequency SET trainid=?,frequencydays=? WHERE id=?";
	
	
	TrainFrequency c = null;
	
	@Override
	public int findNextSeqVal() {
		int x = 0;
		Connection con = null;
		try {
			con = JDBCUtil.getOracleConnection();
			  PreparedStatement stmt;
			 stmt = con.prepareStatement(FINDSEQNEXTVAL);
			  ResultSet rs = stmt.executeQuery();
			  while(rs.next()) {
				  x = rs.getInt(1);
				  
			  }
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x;

	}


	@Override
	public int add(Object obj) {
		int x = 0;
		TrainFrequency d = (TrainFrequency)obj;
	
	  	  Connection con;
			try {
				con = JDBCUtil.getOracleConnection();
				x = findNextSeqVal();
					
				  PreparedStatement ps = con.prepareStatement(INSERT);		
				  ps.setInt(1, x);
			          ps.setInt(2,d.getTrainId());
			          ps.setString(3, d.getFrequencyDays());
			           ps.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	  	return x;
	}


	@Override
	public boolean delete(int id) {
		int x = 0;
		try {
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(DELETE);
			ps.setInt(1, id);
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x==1;
	}


	@Override
	public boolean update(Object obj) {
		TrainFrequency d = (TrainFrequency)obj;
		int x = 0;
		Connection con;
		
		try {
			con = JDBCUtil.getOracleConnection(); 
			
			PreparedStatement ps = con.prepareStatement(UPDATE);
			  ps.setInt(1,d.getTrainId());
	          ps.setString(2, d.getFrequencyDays()); 
		          ps.setInt(3, d.getId());
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}		
		//JDBCUtil.cleanup(ps, con);
		return x==1;	
		}


	@Override
	public List<Object> getAll() {
		Connection con = null;
		PreparedStatement stmt = null;
		List<Object> list = new ArrayList<>();
		
		try {
			con = JDBCUtil.getOracleConnection();
			stmt = con.prepareStatement(FIND_ALL);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				c = new TrainFrequency();
				 c.setId(rs.getInt("id"));
				c.setTrainId(rs.getInt("trainid"));
				c.setFrequencyDays(rs.getString("frequencydays"));;
				list.add(c);
			}
		} catch (SQLException e) {
			// e.printStackTrace();
			throw new RuntimeException(e);
		} finally {			
		}		
		return list;
	}


	@Override
	public TrainFrequency getTrainFreqById(int id) throws SQLException {
		TrainFrequency c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
			ps.setInt(1, id);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new TrainFrequency();
				 c.setId(rs.getInt("id"));
				c.setTrainId(rs.getInt("trainid"));
				c.setFrequencyDays(rs.getString("frequencydays"));
					
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;
	}


	@Override
	public List<TrainFrequency> getTrainFreuencyByTrainId(int trainId) {
		
		TrainFrequency c = null;
		ResultSet rs = null;
		List<TrainFrequency> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_TRAINID);
			ps.setInt(1, trainId);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new TrainFrequency();
				c.setId(rs.getInt("id"));
				c.setTrainId(rs.getInt("trainid"));
				c.setFrequencyDays(rs.getString("frequencydays"));
				
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}


	@Override
	public List<TrainFrequency> getTrainFrequencyByDays(String frequencyDays) {
		TrainFrequency c = null;
		ResultSet rs = null;
		List<TrainFrequency> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_FREQUENCYDAYS);
			ps.setString(1, frequencyDays);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new TrainFrequency();
				c.setId(rs.getInt("id"));
				c.setTrainId(rs.getInt("trainid"));
				c.setFrequencyDays(rs.getString("frequencydays"));
				
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

}
