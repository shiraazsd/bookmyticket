package com.bookmytkt.Newdao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.bookmytkt.InterfaceDao.UserDao;
import com.bookmytkt.dto.Traintable;
import com.bookmytkt.dto.User;

import JDBCUtil.JDBCUtil;

public class UserDaoImpl implements UserDao{
	
	private static final String FINDSEQNEXTVAL = "SELECT userseq.nextval from dual";

	
	private static final String DELETE = "delete from users  where id=?";
	private static final String FIND_ALL = "SELECT * FROM users ORDER BY id";
	private static final String FIND_BY_ID = "SELECT * FROM users WHERE id=?";
	
	private static final String FIND_BY_USERNAME = "SELECT * FROM users WHERE username=?";

	private static final String FIND_BY_FIRSTNAME = "SELECT * FROM users WHERE firstname=?";
	private static final String FIND_BY_LASTNAME = "SELECT * FROM users WHERE lastname=?";
	private static final String FIND_BY_ADDRESS = "SELECT * FROM uers WHERE address=?";
	private static final String FIND_BY_CITY = "SELECT * FROM users WHERE city=?";
	private static final String FIND_BY_PHONE = "SELECT * FROM users WHERE phone=?";
	private static final String FIND_BY_EMAIL = "SELECT * FROM users WHERE email=?";
	private static final String FIND_BY_PASSWORD = "SELECT * FROM users WHERE password=?";
	
	private static final String INSERT = "INSERT INTO users VALUES(?, ?,?,?,?,?,?,?)";
	private static final String UPDATE = "UPDATE users SET firstname=?,lastname=?,address=? ,city=?,email=?,phoneno=?,password=? WHERE id=?";
	private static final String LOGIN = "select username,password from users where username=? and password=?";
	
	
	
	public static boolean checkUnique(String cond ,String val) {

		 Connection con = null;
		// ResultSet rs3 = null;
		PreparedStatement ps ;
		String qry = null;
		
		try {
			con = JDBCUtil.getOracleConnection();
			if(cond=="username")
			 qry = "select username  from users where username=? ";
			else if(cond=="email")
				qry = "select email  from users where email=?";
			else
				qry = "select phoneno  from users where phoneno=?";
			ps = con.prepareStatement(qry);
			
			ps.setString(1, val);
			ResultSet rs = ps.executeQuery();		
			while(rs.next()) {			
				return true;
			}			
			ps = con.prepareStatement(qry);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// JDBCUtil.cleanup(ps, con);
		return false;
	}
	
	public static void changePassword(String userName, String password)  {

		Connection con;
		
		try {	
			con = JDBCUtil.getOracleConnection();
			PreparedStatement ps = con.prepareStatement("update users set password=? where username=?");
		ps.setString(1, password);
		ps.setString(2, userName);

		ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// con = JDBCUtil.getOracleConnection();
	
	}

	
	public static boolean logIn(String name, String password)
			 {

		 Connection con;
		 boolean cond = false;

		 try {
			con = JDBCUtil.getOracleConnection();
			

			PreparedStatement ps = con.prepareStatement(LOGIN);
			ps.setString(1, name);
			ps.setString(2, password);
			ResultSet rs3 = ps.executeQuery();
			if (rs3.next()) {
				rs3.getString("username");
				password = rs3.getString("password");
				cond = true;
			} else
				cond = false;
		 } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cond;
	}
	
	@Override
	public int findNextSeqVal() {
		int x = 0;
		Connection con = null;
		try {
			con = JDBCUtil.getOracleConnection();
			  PreparedStatement stmt;
			 stmt = con.prepareStatement(FINDSEQNEXTVAL);
			  ResultSet rs = stmt.executeQuery();
			  while(rs.next()) {
				  x = rs.getInt(1);
				  
			  }
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x;

	}
	
	@Override
	public int add(Object obj) {
		int x = 0;
		User d = (User)obj;
	
	  	  Connection con;
			try {
				con = JDBCUtil.getOracleConnection();
				
				x = findNextSeqVal();
					
				  PreparedStatement ps = con.prepareStatement(INSERT);		
				  ps.setInt(1, x);
			          ps.setString(2,d.getFirstName());
			          ps.setString(3, d.getPassword());
			          ps.setString(4, d.getEmailId());
			          ps.setString(5, d.getPhoneNo());
			          ps.setString(6, d.getLastName());
			          ps.setString(7, d.getAddress());
			          ps.setString(8, d.getCity());
			          
			           ps.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	  	return (x);
	}
	@Override
	public boolean delete(int id) {
		int x = 0;
		try {
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(DELETE);
			ps.setInt(1, id);
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x==1;
	}
	@Override
	public boolean update(Object obj) {
		User d = (User)obj;
		int x = 0;
		Connection con;
		
		try {
			con = JDBCUtil.getOracleConnection(); 
			
			PreparedStatement ps = con.prepareStatement(UPDATE);
		    ps.setString(1,d.getFirstName());
	          ps.setString(2, d.getLastName());
	          ps.setString(3, d.getAddress());
	          ps.setString(4, d.getCity());
	          ps.setString(5, d.getEmailId());
	          ps.setString(6, d.getPhoneNo());
	          ps.setString(7, d.getPassword());
	         
	          ps.setInt(8, d.getId());
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}		
		//JDBCUtil.cleanup(ps, con);
		return x==1;	
	}
	@Override
	public List<Object> getAll() {
		User c = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<Object> list = new ArrayList<>();
		
		try {
			con = JDBCUtil.getOracleConnection();
			stmt = con.prepareStatement(FIND_ALL);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				c = new User();
				 c.setId(rs.getInt("id"));
				c.setFirstName(rs.getString("firstname"));
				c.setLastName(rs.getString("lastname"));;
				c.setAddress(rs.getString("address"));
				c.setCity(rs.getString("city"));
				c.setEmailId(rs.getString("email"));
				c.setPhoneNo(rs.getString("phoneno"));
				c.setPassword(rs.getString("password"));
				
				
				list.add(c);
			}
		} catch (SQLException e) {
			// e.printStackTrace();
			throw new RuntimeException(e);
		} finally {			
		}		
		return list;
	}
	
	@Override
	public User getUserById(int id) throws SQLException {
		User c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
			ps.setInt(1, id);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new User();
				 c.setId(rs.getInt("id"));
					c.setFirstName(rs.getString("firstname"));
					c.setLastName(rs.getString("lastname"));;
					c.setAddress(rs.getString("address"));
					c.setCity(rs.getString("city"));
					c.setEmailId(rs.getString("email"));
					c.setPhoneNo(rs.getString("phoneno"));
					c.setPassword(rs.getString("password"));;
					
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;		
	}
	@Override
	public List<User> getUserByFirstName(String firstName) {
		User c = null;
		ResultSet rs = null;
		List<User> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_FIRSTNAME);
			ps.setString(1, firstName);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new User();
				 c.setId(rs.getInt("id"));
					c.setFirstName(rs.getString("firstname"));
					c.setLastName(rs.getString("lastname"));;
					c.setAddress(rs.getString("address"));
					c.setCity(rs.getString("city"));
					c.setEmailId(rs.getString("email"));
					c.setPhoneNo(rs.getString("phoneno"));
					c.setPassword(rs.getString("password"));;
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;		
	}
	@Override
	public List<User> getUserByLastName(String lastName) {
		User c = null;
		ResultSet rs = null;
		List<User> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_LASTNAME);
			ps.setString(1, lastName);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new User();
				 c.setId(rs.getInt("id"));
					c.setFirstName(rs.getString("firstname"));
					c.setLastName(rs.getString("lastname"));;
					c.setAddress(rs.getString("address"));
					c.setCity(rs.getString("city"));
					c.setEmailId(rs.getString("email"));
					c.setPhoneNo(rs.getString("phoneno"));
					c.setPassword(rs.getString("password"));;
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}
	@Override
	public List<User> getUserByAddress(String address) {
		User c = null;
		ResultSet rs = null;
		List<User> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ADDRESS);
			ps.setString(1, address);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new User();
				 c.setId(rs.getInt("id"));
					c.setFirstName(rs.getString("firstname"));
					c.setLastName(rs.getString("lastname"));;
					c.setAddress(rs.getString("address"));
					c.setCity(rs.getString("city"));
					c.setEmailId(rs.getString("email"));
					c.setPhoneNo(rs.getString("phoneno"));
					c.setPassword(rs.getString("password"));;
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}
	@Override
	public List<User> getUserByCity(String city) {
		User c = null;
		ResultSet rs = null;
		List<User> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_CITY);
			ps.setString(1, city);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new User();
				 c.setId(rs.getInt("id"));
					c.setFirstName(rs.getString("firstname"));
					c.setLastName(rs.getString("lastname"));;
					c.setAddress(rs.getString("address"));
					c.setCity(rs.getString("city"));
					c.setEmailId(rs.getString("email"));
					c.setPhoneNo(rs.getString("phoneno"));
					c.setPassword(rs.getString("password"));;
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}
	@Override
	public List<User> getUserByPhoneNo(String phoneNo) {
		User c = null;
		ResultSet rs = null;
		List<User> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ADDRESS);
			ps.setString(1, phoneNo);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new User();
				 c.setId(rs.getInt("id"));
					c.setFirstName(rs.getString("firstname"));
					c.setLastName(rs.getString("lastname"));;
					c.setAddress(rs.getString("address"));
					c.setCity(rs.getString("city"));
					c.setEmailId(rs.getString("email"));
					c.setPhoneNo(rs.getString("phoneno"));
					c.setPassword(rs.getString("password"));;
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}
	@Override
	public List<User> getUserByEmail(String email) {
		User c = null;
		ResultSet rs = null;
		List<User> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_EMAIL);
			ps.setString(1, email);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new User();
				 c.setId(rs.getInt("id"));
					c.setFirstName(rs.getString("firstname"));
					c.setLastName(rs.getString("lastname"));;
					c.setAddress(rs.getString("address"));
					c.setCity(rs.getString("city"));
					c.setEmailId(rs.getString("email"));
					c.setPhoneNo(rs.getString("phoneno"));
					c.setPassword(rs.getString("password"));;
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}
	@Override
	public List<User> getUserByPassword(String password) {
		User c = null;
		ResultSet rs = null;
		List<User> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_PASSWORD);
			ps.setString(1, password);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new User();
				 c.setId(rs.getInt("id"));
					c.setFirstName(rs.getString("firstname"));
					c.setLastName(rs.getString("lastname"));;
					c.setAddress(rs.getString("address"));
					c.setCity(rs.getString("city"));
					c.setEmailId(rs.getString("email"));
					c.setPhoneNo(rs.getString("phoneno"));
					c.setPassword(rs.getString("password"));;
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}
	@Override
	public User getUserByUserName(String userName) throws SQLException {
		
		User c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_USERNAME);
			ps.setString(1, userName);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new User();
				 c.setId(rs.getInt("id"));
				 c.setFirstName(rs.getString("username"));
					
					c.setFirstName(rs.getString("firstname"));
					
					c.setLastName(rs.getString("lastname"));;
					c.setAddress(rs.getString("address"));
					c.setCity(rs.getString("city"));
					c.setEmailId(rs.getString("email"));
					c.setPhoneNo(rs.getString("phoneno"));
					c.setPassword(rs.getString("password"));;
					
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;		
	
	}
	
	

}
