package com.bookmytkt.Newdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bookmytkt.InterfaceDao.UserReservationDao;
import com.bookmytkt.dto.City;
import com.bookmytkt.dto.User;
import com.bookmytkt.dto.UserReservation;

import JDBCUtil.JDBCUtil;

public class UserReservationDaoImpl implements UserReservationDao{
	
	
	private static final String FIND_ALL = "SELECT * FROM userReservation ORDER BY id";
	private static final String FIND_BY_ID = "SELECT * FROM userReservation WHERE id=?";
	private static final String FIND_BY_USERID = "SELECT * FROM userReservation WHERE userid=?";
	private static final String FIND_BY_RESERVATIONID = "SELECT * FROM userReservation WHERE reservationid=?";
	private static final String FINDSEQNEXTVAL = "SELECT userreservseq.nextval from dual";


	
	private static final String INSERT = "INSERT INTO userReservation VALUES(?, ?, ?)";
	private static final String UPDATE = "UPDATE userReservation SET userid=?, passengerid=? WHERE id=?";
    private static final String DELETE = "delete from userReservation  where id=?";
    
    
    private static final String PRINTTKT = "select trainname ,train_no,journeydate,status, pnr_no ,dept_station ,arr_station ,dept_time,arr_time"
    		+ "from  traintable_res c inner join reservation22 a on c.id=a.train_id"
    		+ "inner join (select arrival.trainid,station_dept.stationname as dept_station, station_arr.stationname as arr_station,"
    		+ "departure.time as dept_time, arrival.time as arr_time "
    		+ "from trainroute departure inner join trainroute arrival on departure.trainid = arrival.trainid "
    		+ "inner join station_res station_arr on departure.stationid = station_arr.id inner join station_res station_dept on arrival.stationid = station_dept.id"
    		+ "where  arrival.TRAINID = ? and departure.stationid = ? and arrival.stationid= ? ) b on  a.TRAIN_ID = b.trainid";

    
    public void printTicket(int trainId,int srcId,int destId) {
    	
    	UserReservation c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(PRINTTKT);
			ps.setInt(1, trainId);
			ps.setInt(1, srcId);
			ps.setInt(1, destId);

			
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new UserReservation();
				c.setId(rs.getInt("id"));
				c.setUserId(rs.getInt("userid"));
				c.setReservationId(rs.getInt("reservationid"));
				
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;
    	
    	
    }


	@Override
	public int add(Object obj) {
		UserReservation d = (UserReservation)obj;
		int x = 0;
	  	  Connection con;
			try {
				con = JDBCUtil.getOracleConnection();
				
				  PreparedStatement ps = con.prepareStatement(INSERT);
			  		 // Statement stmt = con.createStatement();
				  x = findNextSeqVal();
			         ps.setInt(1, x);
			          ps.setInt(2, d.getUserId());
			          ps.setInt(3, d.getReservationId());		
			           ps.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	  	return x;
	}

	@Override
	public boolean delete(int id) {
		int x = 0;
		try {
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(DELETE);
			ps.setInt(1, id);
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x==1;
	}

	@Override
	public boolean update(Object obj) {
		int x = 0;
		UserReservation d = (UserReservation)obj;		
		Connection con;
		
		try {
			con = JDBCUtil.getOracleConnection(); 
			
			PreparedStatement ps = con.prepareStatement(UPDATE);
			ps.setInt(1, d.getUserId());
			ps.setInt(2, d.getReservationId());
			ps.setInt(3, d.getId());		
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}		
		//JDBCUtil.cleanup(ps, con);
		return x==1;
	}

	@Override
	public List<Object> getAll() {
		UserReservation c = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<Object> list = new ArrayList<>();
		
		try {
			con = JDBCUtil.getOracleConnection();
			stmt = con.prepareStatement(FIND_ALL);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				c = new UserReservation();
				c.setId(rs.getInt("id"));
				c.setUserId(rs.getInt("userid"));
				c.setReservationId(rs.getInt("reservationid"));
				
				list.add(c);
			}
		} catch (SQLException e) {
			// e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			
		}
		
		return list;
	}

	@Override
	public int findNextSeqVal() {
		int x = 0;
		Connection con = null;
		try {
			con = JDBCUtil.getOracleConnection();
			  PreparedStatement stmt;
			 stmt = con.prepareStatement(FINDSEQNEXTVAL);
			  ResultSet rs = stmt.executeQuery();
			  while(rs.next()) {
				  x = rs.getInt(1);
				  
			  }
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x;
	}

	@Override
	public UserReservation getUserReservationById(int id) throws SQLException {
		UserReservation c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
			ps.setInt(1, id);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new UserReservation();
				c.setId(rs.getInt("id"));
				c.setUserId(rs.getInt("userid"));
				c.setReservationId(rs.getInt("reservationid"));
				
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;
	}

	@Override
	public UserReservation getUserByReservationId(int reservationId) throws SQLException {
		UserReservation c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_RESERVATIONID);
			ps.setInt(1, reservationId);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new UserReservation();
				c.setId(rs.getInt("id"));
				c.setUserId(rs.getInt("userid"));
				c.setReservationId(rs.getInt("reservationid"));
				
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;
	}

	@Override
	public List<UserReservation> getUserByUserId(int userId) {
		UserReservation c = null;
		ResultSet rs = null;
		List<UserReservation> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_USERID);
			ps.setInt(1, userId);
			 rs = ps.executeQuery();
			System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new UserReservation();
				 c.setId(rs.getInt("id"));
				 c.setUserId(rs.getInt("userid"));
					c.setReservationId(rs.getInt("reservationid"));	
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

}
