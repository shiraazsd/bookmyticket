package com.bookmytkt.Newdao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.bookmytkt.InterfaceDao.CityDao;
import com.bookmytkt.dto.Billing;
import com.bookmytkt.dto.City;

import JDBCUtil.JDBCUtil;

public class CityDaoImpl implements CityDao{
	
	List<City> cities;//list obj  billings created.
    City c = new City();
    
    
	private static final String FIND_ALL = "SELECT * FROM city_res ORDER BY id";
	private static final String FIND_BY_ID = "SELECT * FROM city_res WHERE id=?";
	private static final String FIND_BY_NAME = "SELECT * FROM city_res WHERE name=?";
	private static final String FIND_BY_METROCITY = "SELECT * FROM city_res WHERE metrocity=?";
	private static final String FINDSEQNEXTVAL = "SELECT cityseq.nextval from dual";


	
	private static final String INSERT = "INSERT INTO city_res VALUES(?, ?, ?)";
	private static final String UPDATE = "UPDATE city_res SET name=?, metrocity=? WHERE id=?";
    private static final String DELETE = "delete from city_res  where id=?";


	

	@Override
	public int findNextSeqVal() {
		int x = 0;
		Connection con = null;
		try {
			con = JDBCUtil.getOracleConnection();
			  PreparedStatement stmt;
			 stmt = con.prepareStatement(FINDSEQNEXTVAL);
			  ResultSet rs = stmt.executeQuery();
			  while(rs.next()) {
				  x = rs.getInt(1);
				  
			  }
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x;

	}
	
	@Override
	public int add(Object obj) {
		City d = (City)obj;
		int x = 0;
	  	  Connection con;
			try {
				con = JDBCUtil.getOracleConnection();
				
				  PreparedStatement ps = con.prepareStatement(INSERT);
			  		 // Statement stmt = con.createStatement();
				  x = findNextSeqVal();
			         ps.setInt(1, x);
			          ps.setString(2, d.getName());
			          ps.setString(3, d.getMetroCity());		
			           ps.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	  	return x;
	}

	@Override
	public boolean delete(int id) {
		int x = 0;
		try {
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(DELETE);
			ps.setInt(1, id);
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x==1;
	}

	@Override
	public boolean update(Object obj) {
		int x = 0;
		City d = (City)obj;
		Connection con;
		
		try {
			con = JDBCUtil.getOracleConnection(); 
			
			PreparedStatement ps = con.prepareStatement(UPDATE);
			ps.setString(1, d.getName());
			ps.setString(2, d.getMetroCity());
			ps.setInt(3, d.getId());		
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}		
		//JDBCUtil.cleanup(ps, con);
		return x==1;
	}

	@Override
	public List<Object> getAll() {
		City c = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<Object> list = new ArrayList<>();
		
		try {
			con = JDBCUtil.getOracleConnection();
			stmt = con.prepareStatement(FIND_ALL);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				c = new City();
				c.setId(rs.getInt("id"));
				c.setName(rs.getString("name"));
				c.setMetroCity(rs.getString("metrocity"));
				
				list.add(c);
			}
		} catch (SQLException e) {
			// e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			
		}
		
		return list;
		//list returned
	}
	
	
	
	
	
		
	@Override
	public City getCityById(int cityId) throws SQLException {
		
		City c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
			ps.setInt(1, cityId);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new City();
				c.setId(rs.getInt("id"));
				c.setName(rs.getString("name"));
				c.setMetroCity(rs.getString("metrocity"));	
				
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;
		
	}
	@Override
	public City getCityByName(String name) {
		
		City c = null;
		ResultSet rs = null;
		//List<City> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_NAME);
			ps.setString(1, name);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new City();
				 c.setId(rs.getInt("id"));
				c.setName(rs.getString("name"));
				c.setMetroCity(rs.getString("metrocity"));		
				//list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;
		
	}
	@Override
	public List<City> getCityByMetroCity(String metroCity) {
		City c = null;
		ResultSet rs = null;
		List<City> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_METROCITY);
			ps.setString(1, metroCity);
			 rs = ps.executeQuery();
			System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new City();
				 c.setId(rs.getInt("id"));
				c.setName(rs.getString("name"));
				c.setMetroCity(rs.getString("metrocity"));		
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

	



}
