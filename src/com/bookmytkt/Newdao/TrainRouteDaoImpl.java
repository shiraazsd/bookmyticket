package com.bookmytkt.Newdao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bookmytkt.InterfaceDao.TrainFrequencyDao;
import com.bookmytkt.InterfaceDao.TrainRouteDao;
import com.bookmytkt.dto.TrainFrequency;
import com.bookmytkt.dto.TrainRoute;
import com.bookmytkt.dto.Traintable;

import JDBCUtil.JDBCUtil;

public class TrainRouteDaoImpl implements TrainRouteDao{
	
	private static final String DELETE = "delete from newtrainroute  where id=?";
	private static final String FIND_ALL = "SELECT * FROM newtrainroute ORDER BY id";
	private static final String FIND_BY_ID = "SELECT * FROM newtrainroute WHERE id=?";
	private static final String FINDSEQNEXTVAL = "SELECT newtrainrouteseq.nextval from dual";


	private static final String FIND_BY_TRAINID = "SELECT * FROM newtrainroute WHERE trainid=?";
	private static final String FIND_BY_STATIONID = "SELECT * FROM newtrainroute WHERE stationid=?";
	private static final String FIND_BY_ARRIVALTIME = "SELECT * FROM newtrainroute WHERE arrivaltime=?";
	private static final String FIND_BY_DEPARTURETIME = "SELECT * FROM newtrainroute WHERE departuretime=?";
	
	private static final String INSERT = "INSERT INTO newtrainroute VALUES(?, ?,?,?,?)";
	private static final String UPDATE = "UPDATE newtrainroute SET trainid=?,stationid=?,arrivaltime=?,departuretime=? WHERE id=?";
	
	
	public static ArrayList<String> getSrcAndDestSationName(int srcId,int DestId){
	Connection con;
	  //	int trainId = 0;
	ArrayList<String> al = new ArrayList<>();
	try {
		con = JDBCUtil.getOracleConnection();

	  	Statement stmt1 = con.createStatement();
		ResultSet rs1 = stmt1.executeQuery("select stationname from Station_Res "
						+ "where ID = " + srcId + "or id=" + DestId);
		
		while (rs1.next()){
			al.add(rs1.getString("stationname"));
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
	return al;
	  	
	}
	
	public static ArrayList<String> Display(int srcId,int DestId){
	  	Connection con;
	  //	int trainId = 0;
	
	  	ArrayList<String> al = new ArrayList<>();
	  	try {	  		
			con = JDBCUtil.getOracleConnection();			
			Statement stmt = con.createStatement();
			PreparedStatement ps1 = con.prepareStatement("select trainid1,trainname,noofseats,train_no,x.departtime,"
					+ "  x.arrivtime from TRAINTABLE_RES y inner join"
					+ "(select q1.trainid as trainid1,q2.trainid as trainid2,q1.departuretime as departtime,q2.arrivaltime as arrivtime "
					+ "from (select trainid,time as departuretime   from trainroute  where  sTATIONID =?)q1,"
					+ "  (select trainid,time as arrivaltime  from trainroute  where  sTATIONID =?)q2 "
					+ " )x on y.id = x.trainid1 and y.id =x.trainid2 ");
			ps1.setInt(1, srcId);
			ps1.setInt(2, DestId);
			//ps1.setInt(3, 201);
			Date depttime = null;
			Date arrivtime = null;
			
			ResultSet rs = ps1.executeQuery();
			Date d1 = null;
			Date d2 = null;
			while (rs.next()) {			
				String trainId =  String.valueOf(rs.getInt("trainid1"));
				String trainNo = String.valueOf(rs.getInt("train_no"));
				String noOfSeats = String.valueOf(rs.getInt("noofseats"));
				String arrivalTime = String.valueOf(rs.getTime("arrivtime"));
				String departureTime = String.valueOf(rs.getTime("departtime"));
				depttime = rs.getTime("departtime");
				arrivtime = rs.getTime("arrivtime");	
				 //trainId = rs.getInt("trainid");
				al.add(trainNo);
	            al.add(rs.getString("trainname"));
	            al.add(trainId);
				al.add(noOfSeats);
				al.add(arrivalTime);
				al.add(departureTime);
				
				Statement stmt1 = con.createStatement();
				ResultSet rs1 = stmt1.executeQuery("select stationname from Station_Res "
								+ "where ID = " + srcId + "or id=" + DestId);
				while (rs1.next()) {
					al.add(rs1.getString("stationname"));
				}
				
				
				long diff =  arrivtime.getTime()-depttime.getTime();
				long diffHours = diff / (60 * 60 * 1000) % 24;
				long diffMinutes = diff / (60 * 1000) % 60;
				String timeInHour = String.valueOf(diffHours )+"hr";
				String timeInMin = String.valueOf(diffMinutes )+"min";
				al.add(timeInHour);
				al.add(timeInMin);
				al.add("\n");
				
				
			}
			
			
			/*
			Statement stmt3 = con.createStatement();
			trainId = 201;
			
			PreparedStatement ps = con.prepareStatement("select q1.departuretime as depttime,q2.arrivaltime as arrivtime  from " + 
					"  (select trainid ,Time  as departureTime  from trainroute  where  sTATIONID =?)q1," + 
					"  (select trainid,Time  as arrivalTime from trainroute  where  sTATIONID =?)q2 " + 
					"  where q1.trainid=q2.trainid and q1.trainid =?");
			ps.setInt(1, srcId);
			ps.setInt(2, DestId);
			ps.setInt(3, trainId);
			ResultSet rs3 = ps.executeQuery();
			*/

/*
			while (rs3.next()) {
				depttime = rs3.getTime("depttime");
				arrivtime = rs3.getTime("arrivaltime");	
				String arrivalTime = String.valueOf(rs.getTime("arrivaltime"));
				String departureTime = String.valueOf(rs.getTime("depttime"));
				al.add(arrivalTime);
				al.add(departureTime);
				*/
			
		//	}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return al;
	}
	
	
	@Override
	public int findNextSeqVal() {
		int x = 0;
		Connection con = null;
		try {
			con = JDBCUtil.getOracleConnection();
			  PreparedStatement stmt;
			 stmt = con.prepareStatement(FINDSEQNEXTVAL);
			  ResultSet rs = stmt.executeQuery();
			  while(rs.next()) {
				  x = rs.getInt(1);
				  
			  }
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x;

		 
	}
	
	@Override
	public int add(Object obj) {
		int x = 0;
		TrainRoute d = (TrainRoute)obj;
	
	  	  Connection con;
			try {
				con = JDBCUtil.getOracleConnection();
				
					
				  PreparedStatement ps = con.prepareStatement(INSERT);
				  x = findNextSeqVal();
				  ps.setInt(1, x);
			       ps.setInt(2,d.getTrainId());
			          ps.setInt(3, d.getStationId());
			          ps.setString(4, d.getArrivalTime());
			          ps.setString(5, d.getDepartureTime());
			           ps.executeUpdate();   
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	  	return x;
	}
	@Override
	public boolean delete(int id) {
		int x = 0;
		try {
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(DELETE);
			ps.setInt(1, id);
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x==1;
	}
	@Override
	public boolean update(Object obj) {
		TrainRoute d = (TrainRoute)obj;
		int x = 0;
		Connection con;
		
		try {
			con = JDBCUtil.getOracleConnection(); 
			
			PreparedStatement ps = con.prepareStatement(UPDATE);
			 ps.setInt(1,d.getTrainId());
	          ps.setInt(2, d.getStationId());
	          ps.setString(3, d.getArrivalTime());
	          ps.setString(4, d.getDepartureTime());
	          ps.setInt(5, d.getId());
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}		
		//JDBCUtil.cleanup(ps, con);
		return x==1;	
	}
	@Override
	public List<Object> getAll() {
		TrainRoute c = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<Object> list = new ArrayList<>();
		
		try {
			con = JDBCUtil.getOracleConnection();
			stmt = con.prepareStatement(FIND_ALL);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				c = new TrainRoute();
				 c.setId(rs.getInt("id"));
				 c.setTrainId(rs.getInt("trainid"));
				c.setStationID(rs.getInt("stationid"));
				c.setArrivalTime(rs.getString("arrivaltime"));
				c.setDepartureTime(rs.getString("departuretime"));
				
				list.add(c);
			}
		} catch (SQLException e) {
			// e.printStackTrace();
			throw new RuntimeException(e);
		} finally {			
		}		
		return list;
	}
	@Override
	public TrainRoute getTrainRouteById(int id) throws SQLException {
		TrainRoute c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
			ps.setInt(1, id);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new TrainRoute();
				
				 c.setId(rs.getInt("id"));
				 c.setTrainId(rs.getInt("trainid"));
					c.setStationID(rs.getInt("stationid"));
					c.setArrivalTime(rs.getString("arrivaltime"));
					c.setDepartureTime(rs.getString("departuretime"));
					
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;		
	}
	@Override
	public List<TrainRoute> getTrainRouteByTrainId(int trainId) {
		TrainRoute c = null;
		ResultSet rs = null;
		List<TrainRoute> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_TRAINID);
			ps.setInt(1, trainId);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new TrainRoute();					
				 c.setId(rs.getInt("id"));
				 c.setTrainId(rs.getInt("trainid"));
					c.setStationID(rs.getInt("stationid"));
					c.setArrivalTime(rs.getString("arrivaltime"));
					c.setDepartureTime(rs.getString("departuretime"));
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;	
	}
	
	@Override
	public List<TrainRoute> getTrainRouteByArrivalTime(String arrivalTime) {
		TrainRoute c = null;
		ResultSet rs = null;
		List<TrainRoute> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ARRIVALTIME);
			ps.setString(1, arrivalTime);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new TrainRoute();					
				 c.setId(rs.getInt("id"));
				 c.setTrainId(rs.getInt("trainid"));
					c.setStationID(rs.getInt("stationid"));
					c.setArrivalTime(rs.getString("arrivaltime"));
					c.setDepartureTime(rs.getString("departuretime"));
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}
	@Override
	public List<TrainRoute> getTrainRouteByDepartureTime(String departureTime) {
		TrainRoute c = null;
		ResultSet rs = null;
		List<TrainRoute> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_DEPARTURETIME);
			ps.setString(1, departureTime);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new TrainRoute();					
				 c.setId(rs.getInt("id"));
				 c.setTrainId(rs.getInt("trainid"));
					c.setStationID(rs.getInt("stationid"));
					c.setArrivalTime(rs.getString("arrivaltime"));
					c.setDepartureTime(rs.getString("departuretime"));
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}
	@Override
	public List<TrainRoute> getTrainRouteByStationId(int StaionId) {
		TrainRoute c = null;
		ResultSet rs = null;
		List<TrainRoute> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_STATIONID);
			ps.setInt(1, StaionId);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new TrainRoute();					
				 c.setId(rs.getInt("id"));
				 c.setTrainId(rs.getInt("trainid"));
					c.setStationID(rs.getInt("stationid"));
					c.setArrivalTime(rs.getString("arrivaltime"));
					c.setDepartureTime(rs.getString("departuretime"));
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}


	
	
	
	
}


