package com.bookmytkt.Newdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bookmytkt.InterfaceDao.PassengerDao;
import com.bookmytkt.dto.City;
import com.bookmytkt.dto.DocId;
import com.bookmytkt.dto.Passenger;
import com.bookmytkt.dto.Reservation;
import com.mysql.jdbc.Statement;

import JDBCUtil.JDBCUtil;

public class PassengerDaoImpl implements PassengerDao {
	

	
	private static final String DELETE = "delete from passeng_res  where id=?";
	private static final String FIND_ALL = "SELECT * FROM passeng_res ORDER BY id";
	private static final String FIND_BY_ID = "SELECT * FROM passeng_res WHERE id=?";
	private static final String FIND_BY_PASSENGERNAME = "SELECT * FROM passeng_res WHERE passengername=?";
	private static final String FIND_BY_DOB = "SELECT * FROM passeng_res WHERE dob=?";
	private static final String FIND_BY_DOCID = "SELECT * FROM passeng_res WHERE doc_id=?";
	private static final String FIND_BY_ADDRESSID = "SELECT * FROM passeng_res WHERE address_id=?";
	private static final String FINDSEQNEXTVAL = "SELECT passengseq.nextval from dual";


	private static final String INSERT = "INSERT INTO passeng_res VALUES(?, ?,?,?,?)";
	private static final String UPDATE = "UPDATE passeng_res SET passengername=?,dob=?,doc_id=?,address_id=? WHERE id=?";
	

	
	
	
	@Override
	public int findNextSeqVal() {
		int x = 0;
		Connection con = null;
		try {
			con = JDBCUtil.getOracleConnection();
			  PreparedStatement stmt;
			 stmt = con.prepareStatement(FINDSEQNEXTVAL);
			  ResultSet rs = stmt.executeQuery();
			  while(rs.next()) {
				  x = rs.getInt(1);
				  
			  }
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x;
	}


	@Override
	public int add(Object obj) {
		int x = 0;
		Passenger d = (Passenger)obj;
	  	  Connection con;
	  	  PreparedStatement stmt;
			try {
				con = JDBCUtil.getOracleConnection();
				  PreparedStatement ps = con.prepareStatement(INSERT);
				  
				  //stmt = con.prepareStatement(FIND_ALL);
				  x = findNextSeqVal();
				 
				  ps.setInt(1, x);
			          ps.setString(2, d.getPassengerName());
			          ps.setDate(3, (java.sql.Date) d.getDOB());
			          ps.setInt(4, d.getDocId());
			          ps.setInt(5, d.getAddressID());
			         ps.executeUpdate();
			        
			          //
			          //System.out.println("add"+x);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	  	return x;
	}

	@Override
	public boolean delete(int id) {
		int x = 0;
		try {
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(DELETE);
			ps.setInt(1, id);
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x==1;
	}
	
	

	
	
	

	@Override
	public boolean update(Object obj) {

		Passenger d = (Passenger)obj;
		int x = 0;
		Connection con;
		
		try {
			con = JDBCUtil.getOracleConnection(); 
			
			PreparedStatement ps = con.prepareStatement(UPDATE);
			ps.setInt(5, d.getId());
			 ps.setString(1, d.getPassengerName());
	          ps.setDate(2, (java.sql.Date) d.getDOB());
	          ps.setInt(3, d.getDocId());
	          ps.setInt(4, d.getAddressID());			
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}		
		//JDBCUtil.cleanup(ps, con);
		return x==1;
	}

	@Override
	public List<Object> getAll() {
		Connection con = null;
		PreparedStatement stmt = null;
		List<Object> list = new ArrayList<>();
		
		try {
			con = JDBCUtil.getOracleConnection();
			stmt = con.prepareStatement(FIND_ALL);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				Passenger c = new Passenger();
				c.setId(rs.getInt("id"));
				c.setPassengerName(rs.getString("passengername"));
				c.setDate(rs.getDate("dob"));//date converted into string
				c.setDocId(rs.getInt("doc_id"));
				c.setAddressID(rs.getInt("address_id"));				
				list.add(c);
			}
		} catch (SQLException e) {
			// e.printStackTrace();
			throw new RuntimeException(e);
		} finally {			
		}		
		return list;
	}

	@Override
	public Passenger getPassengerById(int id) throws SQLException {
		Passenger c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
			ps.setInt(1, id);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Passenger();
				c.setId(rs.getInt("id"));
				c.setPassengerName(rs.getString("passengername"));
				c.setDate(rs.getDate("dob"));//date converted into string
				c.setDocId(rs.getInt("doc_id"));
				c.setAddressID(rs.getInt("address_id"));
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;
	}

	@Override
	public List<Passenger> getPassengerByPassengerName(String passengerName) {
		Passenger c = null;
		ResultSet rs = null;
		List<Passenger> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_PASSENGERNAME);
			ps.setString(1, passengerName);
			 rs = ps.executeQuery();
		
			while(rs.next()) {
				
				 c = new Passenger();
				 c.setId(rs.getInt("id"));
					c.setPassengerName(rs.getString("passengername"));
					c.setDate(rs.getDate("dob"));//date converted into string
					c.setDocId(rs.getInt("doc_id"));
					c.setAddressID(rs.getInt("address_id"));
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

	@Override
	public List<Passenger> getPassengerByDOB(Date dob) {
		Passenger c = null;
		ResultSet rs = null;
		List<Passenger> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_DOB);
			ps.setDate(1, (java.sql.Date) dob);
			 rs = ps.executeQuery();
		
			while(rs.next()) {
				
				 c = new Passenger();
				 c.setId(rs.getInt("id"));
					c.setPassengerName(rs.getString("passengername"));
					c.setDate(rs.getDate("dob"));//date converted into string
					c.setDocId(rs.getInt("doc_id"));
					c.setAddressID(rs.getInt("address_id"));
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

	@Override
	public List<Passenger> getPassengerByDocId(int docId) {
		Passenger c = null;
		ResultSet rs = null;
		List<Passenger> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_DOCID);
			ps.setInt(1, docId);
			 rs = ps.executeQuery();
			while(rs.next()) {
				
				 c = new Passenger();
				 c.setId(rs.getInt("id"));
					c.setPassengerName(rs.getString("passengername"));
					c.setDate(rs.getDate("dob"));//date converted into string
					c.setDocId(rs.getInt("doc_id"));
					c.setAddressID(rs.getInt("address_id"));
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

	@Override
	public List<Passenger> getPassengerByAddressId(int AddressId) {
		Passenger c = null;
		ResultSet rs = null;
		List<Passenger> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ADDRESSID);
			ps.setInt(1, AddressId);
			 rs = ps.executeQuery();
		
			while(rs.next()) {
				
				 c = new Passenger();
				 c.setId(rs.getInt("id"));
					c.setPassengerName(rs.getString("passengername"));
					c.setDate(rs.getDate("dob"));//date converted into string
					c.setDocId(rs.getInt("doc_id"));
					c.setAddressID(rs.getInt("address_id"));
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}




	

}
