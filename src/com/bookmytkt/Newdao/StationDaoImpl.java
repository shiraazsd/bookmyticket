package com.bookmytkt.Newdao;	
	import java.sql.Connection;
	import JDBCUtil.*;
	import java.sql.DriverManager;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.sql.Statement;
	import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.bookmytkt.dto.*;
	import com.bookmytkt.InterfaceDao.*;
	
	public class StationDaoImpl  implements StationDao{

	    List<Billing> billings;//list obj  billings created.
	    Billing b = new Billing();
	    
		private static final String FINDSEQNEXTVAL = "SELECT stationseq.nextval from dual";

	    
		private static final String FIND_ALL = "SELECT * FROM station_res ORDER BY id";
		private static final String FIND_BY_ID = "SELECT * FROM station_res WHERE id=?";
		private static final String FIND_BY_CITYID = "SELECT * FROM station_res WHERE city_id=?";
		private static final String FIND_BY_STATIONNAME = "SELECT * FROM station_res WHERE stationname=?";

		
		private static final String INSERT = "INSERT INTO station_res VALUES(?, ?, ?)";
		private static final String UPDATE = "UPDATE station_res SET reserv_id=?, amt=? WHERE id=?";
	    private static final String DELETE = "delete from station_res  where id=?";
    
	   
		
						
		@Override
		public Station getstationById(int stationId) throws SQLException {
			Station st = null;
			ResultSet rs = null;
			
			try {
				
				Connection con = JDBCUtil.getOracleConnection(); 
				PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
				ps.setInt(1, stationId);
				 rs = ps.executeQuery();
				// System.out.println(rs.next());
				while(rs.next()) {
					
					st  = new Station();
					st.setId(rs.getInt("id"));
					st.setStationName(rs.getString("stationname"));
					st.setCityId(rs.getInt("city_id"));	
					
				}		
			} catch (Exception e) {
				// TODO: handle exception
			}			
			return st;
		}
		


		
		@Override
		public Station getStationByCityId(int cityId) {
			Station st = null;
			ResultSet rs = null;
			
			try {
				
				Connection con = JDBCUtil.getOracleConnection(); 
				PreparedStatement ps = con.prepareStatement(FIND_BY_CITYID);
				ps.setInt(1, cityId);
				 rs = ps.executeQuery();
				// System.out.println(rs.next());
				while(rs.next()) {
					
					st  = new Station();
					st.setId(rs.getInt("id"));
					st.setStationName(rs.getString("stationname"));
					st.setCityId(rs.getInt("city_id"));	
					
				}		
			} catch (Exception e) {
				// TODO: handle exception
			}			
			return st;
		}

		@Override
		public Station getStationByStationName(String  stationName) {
			Station st = null;
			ResultSet rs = null;
			
			try {
				
				Connection con = JDBCUtil.getOracleConnection(); 
				PreparedStatement ps = con.prepareStatement(FIND_BY_STATIONNAME);
				ps.setString(1, stationName);
				 rs = ps.executeQuery();
				// System.out.println(rs.next());
				while(rs.next()) {
					
					st  = new Station();
					st.setId(rs.getInt("id"));
					st.setStationName(rs.getString("stationname"));
					st.setCityId(rs.getInt("city_id"));	
					
				}		
			} catch (Exception e) {
				// TODO: handle exception
			}			
			return st;
		}
		
		@Override
		public int findNextSeqVal() {
			int x = 0;
			Connection con = null;
			try {
				con = JDBCUtil.getOracleConnection();
				  PreparedStatement stmt;
				 stmt = con.prepareStatement(FINDSEQNEXTVAL);
				  ResultSet rs = stmt.executeQuery();
				  while(rs.next()) {
					  x = rs.getInt(1);
					  
				  }
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			return x;

		}



		@Override
		public int add(Object obj) {
			int x = 0;
			Station st = (Station)obj;
	    	  Connection con;
			try {
				con = JDBCUtil.getOracleConnection();
				x = findNextSeqVal();
				  PreparedStatement ps = con.prepareStatement(INSERT);
			  		 // Statement stmt = con.createStatement();
			          ps.setInt(1, x);
			          ps.setString(1, st.getStationName());
			          ps.setInt(2,st.getCityId());		
			          ps.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	    	return x;  
		}



		@Override
		public boolean delete(int id) {
			int x = 0;

			try {
				Connection con = JDBCUtil.getOracleConnection(); 
				PreparedStatement ps = con.prepareStatement(DELETE);
				ps.setInt(1, id);
				x = ps.executeUpdate();
			} catch (Exception e) {
				// TODO: handle exception
			}
			return x==1;
				
				//JDBCUtil.cleanup(ps, con);
		}



		@Override
		public boolean update(Object obj) {
			int x = 0;
			Station st = (Station)obj;

			Connection con;
			
			try {
				con = JDBCUtil.getOracleConnection(); 
				
				PreparedStatement ps = con.prepareStatement(UPDATE);
				ps.setInt(1, st.getId());
				ps.setString(2, st.getStationName());
				ps.setInt(3, st.getCityId());		
				x = ps.executeUpdate();
			} catch (Exception e) {
				// TODO: handle exception
			}		
			//JDBCUtil.cleanup(ps, con);
			return x==1;
		}

		@Override
		public List<Object> getAll() {
			Connection con = null;
			PreparedStatement stmt = null;
			List<Object> list = new ArrayList<>();
			
			try {
				con = JDBCUtil.getOracleConnection();
				stmt = con.prepareStatement(FIND_ALL);
				ResultSet rs = stmt.executeQuery();
				
				while (rs.next()) {
					Station st = new Station();
					st.setId(rs.getInt("id"));
					st.setStationName(rs.getString("stationname"));
					st.setCityId(rs.getInt("city_id"));
					
					list.add(st);
				}
			} catch (SQLException e) {
				// e.printStackTrace();
				throw new RuntimeException(e);
			} finally {
				
			}
			
			return list;
		}

		
	
	   
	}
	 


