package com.bookmytkt.Newdao;
import java.sql.Connection;
import JDBCUtil.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.bookmytkt.dto.*;
import com.bookmytkt.InterfaceDao.*;

public class BillingDaoImpl implements BillingDao
{
    List<Billing> billings;//list obj  billings created.
    Billing b = new Billing();
    
    
    private static final String DELETE = "delete from billing_res  where id=?";
	private static final String FIND_ALL = "SELECT * FROM billing_res ORDER BY id";
	private static final String FIND_BY_ID = "SELECT * FROM billing_res WHERE id=?";
	private static final String FIND_BY_AMT = "SELECT * FROM billing_res WHERE amt=?";
	private static final String INSERT = "INSERT INTO billing_res VALUES(?, ?, ?)";
	private static final String UPDATE = "UPDATE billing_res SET reserv_id=?, amt=? WHERE id=?";
	
	private static final String FINDSEQNEXTVAL = "SELECT billingseq.nextval from dual";

	
	
	
	public static float billing(int trainID, int NoOfTkt) throws SQLException {

		 Connection con = null;
		 int x = 0;
		 float amt = 0.0F;
		// ResultSet rs3 = null;
		// Statement stmt3 = null;
		try {
			 con = JDBCUtil.getOracleConnection();
			PreparedStatement ps = con.prepareStatement("insert into billing_res values(billingseq.nextval,?,?)");

			Statement stmt3 = con.createStatement();

			ResultSet rs3 = stmt3.executeQuery(" select id  from reserv_res where train_id=" + trainID);
			while (rs3.next()) {

				int reservId = rs3.getInt("id");
				ps.setInt(1, reservId);
				ResultSet rs = stmt3.executeQuery(" select fare from traintable_Res where id=" + trainID);
				while (rs.next()) {

					 amt = NoOfTkt * rs.getFloat("fare");
					ps.setFloat(2, amt);
					System.out.println("bill");
				}
				 x = ps.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// JDBCUtil.cleanup(rs3, stmt3, con);}
		}
		return amt;
	}
	    
	@Override
	public int findNextSeqVal() {
		int x = 0;
		Connection con = null;
		try {
			con = JDBCUtil.getOracleConnection();
			  PreparedStatement stmt;
			 stmt = con.prepareStatement(FINDSEQNEXTVAL);
			  ResultSet rs = stmt.executeQuery();
			  while(rs.next()) {
				  x = rs.getInt(1);
				  
			  }
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x;
	}
	
    public int add(Object obj) {
    	
    	int x = 0;
		Billing d = (Billing)obj;
    	  Connection con;
		try {
			con = JDBCUtil.getOracleConnection();
				
			x = findNextSeqVal();
			  PreparedStatement ps = con.prepareStatement(INSERT);
		  		 // Statement stmt = con.createStatement();
		          ps.setInt(1, x);
			 
		          ps.setInt(2, d.getReservationId());
		          ps.setFloat(3, d.getAmount());		
		           ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    	return x;
    	  
    	
    	
    }
    
    
    
	@Override
	public boolean delete(int id) {
		int x = 0;
		try {
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(DELETE);
			ps.setInt(1, id);
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x==1;
			
			//JDBCUtil.cleanup(ps, con);
	}

	@Override
	public boolean update(Object obj) {
		int x = 0;
	
		Billing d = (Billing)obj;
		Connection con;
		
		try {
			con = JDBCUtil.getOracleConnection(); 
			
			PreparedStatement ps = con.prepareStatement(UPDATE);
			ps.setInt(1, d.getReservationId());
			ps.setFloat(2, d.getAmount());
			ps.setInt(3, d.getId());		
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}		
		//JDBCUtil.cleanup(ps, con);
		return x==1;
	}

	@Override
	public List<Object> getAll() {
		Billing d = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<Object> list = new ArrayList<>();
		
		try {
			con = JDBCUtil.getOracleConnection();
			stmt = con.prepareStatement(FIND_ALL);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				d = new Billing();
				d.setId(rs.getInt("id"));
				d.setReservationId(rs.getInt("reserv_id"));
				d.setAmount(rs.getFloat("amt"));
				
				list.add(d);
			}
		} catch (SQLException e) {
			// e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			
		}
		
		return list;
		//list returned
	}

	

	
	

	@Override
	public Billing getBillingById(int billingId) throws SQLException {
		// TODO Auto-generated method stub
		Billing bil = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
			ps.setInt(1, billingId);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 bil = new Billing();
				bil.setId(rs.getInt("id"));
				bil.setReservationId(rs.getInt("reserv_id"));
				bil.setAmount(rs.getFloat("amt"));	
				
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return bil;
		}

	@Override
	public List <Billing> getBillingByReservID(int ReservId) {
		Billing bil = null;
		ResultSet rs = null;
		List<Billing> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
			ps.setInt(1, ReservId);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 bil = new Billing();
				bil.setId(rs.getInt("id"));
				bil.setReservationId(rs.getInt("reserv_id"));
				bil.setAmount(rs.getFloat("amt"));	
				list.add(bil);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
		}

	

	@Override
	public List<Billing> getBillingByAmt(Float amt) {
		Billing bil = null;
		ResultSet rs = null;
		List<Billing> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_AMT);
			ps.setFloat(1, amt);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 bil = new Billing();
				bil.setId(rs.getInt("id"));
				bil.setReservationId(rs.getInt("reserv_id"));
				bil.setAmount(rs.getFloat("amt"));	
				list.add(bil);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}



	


   
}
 