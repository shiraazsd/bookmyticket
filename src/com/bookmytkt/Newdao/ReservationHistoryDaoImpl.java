package com.bookmytkt.Newdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bookmytkt.InterfaceDao.ReservationDao;
import com.bookmytkt.InterfaceDao.ReservationHistoryDao;
import com.bookmytkt.dto.Reservation;
import com.bookmytkt.dto.ReservationHistory;

import JDBCUtil.JDBCUtil;

//done later

/*
public class ReservationHistoryDaoImpl implements ReservationHistoryDao{
	


	private static final String DELETE = "delete from reservationhistory  where id=?";
	private static final String FIND_ALL = "SELECT * FROM reservationhistory ORDER BY id";
	private static final String FIND_BY_ID = "SELECT * FROM reservationhistory WHERE id=?";
	private static final String FIND_BY_PASSENGERID = "SELECT * FROM reservationhistory WHERE passeng_id=?";
	private static final String FIND_BY_TRAINID = "SELECT * FROM reservationhistory WHERE train_id=?";
	private static final String FIND_BY_RESERVATIONDATE = "SELECT * FROM reservationhistory WHERE reservationdate=?";
	private static final String FIND_BY_JOURNEYDATE = "SELECT * FROM reservationhistory WHERE journeydate_id=?";
	private static final String FIND_BY_STATUS = "SELECT * FROM reservationhistory WHERE status=?";
	private static final String FIND_BY_PNRNO = "SELECT * FROM reservationhistory WHERE pnr_no=?";



	private static final String INSERT = "INSERT INTO reservationhistory VALUES(reservseq.nextval, ?,?,?,?,?,?)";
	private static final String UPDATE = "UPDATE reservationhistory SET passeng_id=?,train_id=?,reservationdate=?,journeydate=?,status=?,pnr_no=? WHERE id=?";
	

	@Override
	public int add(Object obj) {
		int x = 0;
		ReservationHistory d = (ReservationHistory)obj;
	  	  Connection con;
			try {
				con = JDBCUtil.getOracleConnection();
				  PreparedStatement ps = con.prepareStatement(INSERT);			  		
			          ps.setInt(1, d.getPassengerId());
			          ps.setInt(2, d.getTrainId());
			          ps.setString(3, d.getReservationDate());
			          ps.setString(4, d.getJourneyDate());
			          ps.setString(5, d.getStatusOfReservation());
			          ps.setString(6, d.getPnrNo());
			          x = ps.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	  	return x;
	}

	@Override
	public int delete(int id) {
		int x = 0;
		try {
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(DELETE);
			ps.setInt(1, id);
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x;
	}

	@Override
	public int update(Object obj) {
		ReservationHistory d = (ReservationHistory)obj;
		int x = 0;
		Connection con;
		
		try {
			con = JDBCUtil.getOracleConnection(); 
			
			PreparedStatement ps = con.prepareStatement(UPDATE);
			ps.setInt(7,d.getId());
			 ps.setInt(1, d.getPassengerId());
	          ps.setInt(2, d.getTrainId());
	          ps.setString(3, d.getReservationDate());
	          ps.setString(4, d.getJourneyDate());
	          ps.setString(5, d.getStatusOfReservation());
	          ps.setString(6, d.getPnrNo());
					
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}		
		//JDBCUtil.cleanup(ps, con);
		return x;
	}

	@Override
	public List<Object> getAll() {
		Connection con = null;
		PreparedStatement stmt = null;
		List<Object> list = new ArrayList<>();
		
		try {
			con = JDBCUtil.getOracleConnection();
			stmt = con.prepareStatement(FIND_ALL);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				ReservationHistory c = new ReservationHistory();
				c.setId(rs.getInt("id"));
				c.setPassengerId(rs.getInt("passeng_id"));
				c.setTrainId(rs.getInt("train_id"));
				c.setReservationDate(rs.getDate("reservationdate").toString());//date converted into string
				c.setJourneyDate(rs.getDate("reservationdate").toString());
				c.setStatusOfReservation(rs.getString("status"));
				c.setPnrNo(rs.getString("pnr_no"));				
				list.add(c);
			}
		} catch (SQLException e) {
			// e.printStackTrace();
			throw new RuntimeException(e);
		} finally {			
		}		
		return list;
	}

	@Override
	public ReservationHistory getReservationById(int id) throws SQLException {
		Reservation c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
			ps.setInt(1, id);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new ReservationHistory();
				c.setId(rs.getInt("id"));
				c.setPassengerId(rs.getInt("passeng_id"));
				c.setTrainId(rs.getInt("train_id"));
				c.setReservationDate(rs.getDate("reservationdate").toString());//date converted into string
				c.setJourneyDate(rs.getDate("reservationdate").toString());
				c.setStatusOfReservation(rs.getString("status"));
				c.setPnrNo(rs.getString("pnr_no"));				
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;
	}

	@Override
	public ReservationHistory getReservationByPnrNo(String pnrNo) {
		ReservationHistory c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_PNRNO);
			ps.setString(1, pnrNo);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Reservation();
				c.setId(rs.getInt("id"));
				c.setPassengerId(rs.getInt("passeng_id"));
				c.setTrainId(rs.getInt("train_id"));
				c.setReservationDate(rs.getDate("reservationdate").toString());//date converted into string
				c.setJourneyDate(rs.getDate("reservationdate").toString());
				c.setStatusOfReservation(rs.getString("status"));
				c.setPnrNo(rs.getString("pnr_no"));				
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;
	}

	@Override
	public List<ReservationHistory> getReservationByPassengerId(int passengerId) {
		ReservationHistory c = null;
		ResultSet rs = null;
		List list = new ArrayList<>();
		try {			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_PASSENGERID);
			ps.setInt(1, passengerId);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new ReservationHistory();
				c.setId(rs.getInt("id"));
				c.setPassengerId(rs.getInt("passeng_id"));
				c.setTrainId(rs.getInt("train_id"));
				c.setReservationDate(rs.getDate("reservationdate").toString());//date converted into string
				c.setJourneyDate(rs.getDate("reservationdate").toString());
				c.setStatusOfReservation(rs.getString("status"));
				c.setPnrNo(rs.getString("pnr_no"));
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

	@Override
	public List<Reservation> getReservationByTrainId(int trainId) {
		Reservation c = null;
		ResultSet rs = null;
		List list = new ArrayList<>();
		try {			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_TRAINID);
			ps.setInt(1, trainId);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Reservation();
				c.setId(rs.getInt("id"));
				c.setPassengerId(rs.getInt("passeng_id"));
				c.setTrainId(rs.getInt("train_id"));
				c.setReservationDate(rs.getDate("reservationdate").toString());//date converted into string
				c.setJourneyDate(rs.getDate("reservationdate").toString());
				c.setStatusOfReservation(rs.getString("status"));
				c.setPnrNo(rs.getString("pnr_no"));
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

	@Override
	public List<Reservation> getReservationByReservationDate(String reservationDate) {
		Reservation c = null;
		ResultSet rs = null;
		List list = new ArrayList<>();
		try {			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_RESERVATIONDATE);
			ps.setString(1, reservationDate);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Reservation();
				c.setId(rs.getInt("id"));
				c.setPassengerId(rs.getInt("passeng_id"));
				c.setTrainId(rs.getInt("train_id"));
				c.setReservationDate(rs.getDate("reservationdate").toString());//date converted into string
				c.setJourneyDate(rs.getDate("reservationdate").toString());
				c.setStatusOfReservation(rs.getString("status"));
				c.setPnrNo(rs.getString("pnr_no"));
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

	@Override
	public List<Reservation> getReservationByJourneyDate(String journeyDate) {
		Reservation c = null;
		ResultSet rs = null;
		List list = new ArrayList<>();
		try {			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_JOURNEYDATE);
			ps.setString(1, journeyDate);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Reservation();
				c.setId(rs.getInt("id"));
				c.setPassengerId(rs.getInt("passeng_id"));
				c.setTrainId(rs.getInt("train_id"));
				c.setReservationDate(rs.getDate("reservationdate").toString());//date converted into string
				c.setJourneyDate(rs.getDate("reservationdate").toString());
				c.setStatusOfReservation(rs.getString("status"));
				c.setPnrNo(rs.getString("pnr_no"));
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

	@Override
	public List<Reservation> getReservationByStatusOfReservation(String status) {
		Reservation c = null;
		ResultSet rs = null;
		List list = new ArrayList<>();
		try {			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_STATUS);
			ps.setString(1, status);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Reservation();
				c.setId(rs.getInt("id"));
				c.setPassengerId(rs.getInt("passeng_id"));
				c.setTrainId(rs.getInt("train_id"));
				c.setReservationDate(rs.getDate("reservationdate").toString());//date converted into string
				c.setJourneyDate(rs.getDate("reservationdate").toString());
				c.setStatusOfReservation(rs.getString("status"));
				c.setPnrNo(rs.getString("pnr_no"));
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	

}
*/