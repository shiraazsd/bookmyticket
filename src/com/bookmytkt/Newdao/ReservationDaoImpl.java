package com.bookmytkt.Newdao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bookmytkt.InterfaceDao.ReservationDao;
import com.bookmytkt.dto.Passenger;
import com.bookmytkt.dto.Reservation;

import JDBCUtil.JDBCUtil;

public class ReservationDaoImpl implements ReservationDao  {
	
	
	private static final String FINDSEQNEXTVAL = "SELECT reservseq.nextval from dual";
	
	private static final String DELETEBYPNR = "delete from reserv_res  where pnr_No=?";



	private static final String DELETE = "delete from reservation22  where id=?";
	private static final String FIND_ALL = "SELECT * FROM reservation22 ORDER BY id";
	private static final String FIND_BY_ID = "SELECT * FROM reservation22 WHERE id=?";
	private static final String FIND_BY_PASSENGERID = "SELECT * FROM reservation22 WHERE passeng_id=?";
	private static final String FIND_BY_TRAINID = "SELECT * FROM reservation22 WHERE train_id=?";
	private static final String FIND_BY_RESERVATIONDATE = "SELECT * FROM reservation22 WHERE reservationdate=?";
	private static final String FIND_BY_JOURNEYDATE = "SELECT * FROM reservation22 WHERE journeydate_id=?";
	private static final String FIND_BY_STATUS = "SELECT * FROM reservation22 WHERE status=?";
	private static final String FIND_BY_PNRNO = "SELECT * FROM reservation22 WHERE pnr_no=?";



	private static final String INSERT = "INSERT INTO reservation22 VALUES(?, ?,?,?,?,?,?,?,?,?,?)";
	private static final String UPDATE = "UPDATE reservation SET passeng_id=?,train_id=?,reservationdate=?,journeydate=?,status=?,pnr_no=?,srcstation=?,deststation=? WHERE id=?";
	
	@Override
	public int findNextSeqVal() {
		int x = 0;
		Connection con = null;
		try {
			con = JDBCUtil.getOracleConnection();
			  PreparedStatement stmt;
			 stmt = con.prepareStatement(FINDSEQNEXTVAL);
			  ResultSet rs = stmt.executeQuery();
			  while(rs.next()) {
				  x = rs.getInt(1);
				  
			  }
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x;

	}
	

	@Override
	public int add(Object obj) {
		int x = 0;
		  PreparedStatement ps;
		Reservation d = (Reservation)obj;
	  	  Connection con;
			try {
				con = JDBCUtil.getOracleConnection();
				
				 
					
					x = findNextSeqVal();
				   ps = con.prepareStatement(INSERT);
				   
				   ps.setInt(1, x);
			          ps.setInt(2, d.getPassengerId());
				   
			          ps.setInt(3, d.getTrainId());
			          ps.setDate(4,  (java.sql.Date) d.getReservationDate());
			          ps.setDate(5, (java.sql.Date) d.getJourneyDate());
			          ps.setString(6, d.getStatusOfReservation());
			          ps.setString(7, d.getPnrNo());
			          ps.setString(8, d.getSrcStation());
			          ps.setString(9, d.getDestStation());
			          ps.setString(10, d.getArrivalTime());
			          ps.setString(11, d.getDepartureTime());
			          
			           ps.executeUpdate();
			          
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	  	return x;
	}

	@Override
	public boolean delete(int id) {
		int x = 0;
		try {
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(DELETE);
			ps.setInt(1, id);
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x==1;
	}
	
	
	
	public boolean deleteByPnr(String pnrNo) {
		int x = 0;
		try {
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(DELETEBYPNR);
			ps.setString(1, pnrNo);
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x==1;
	}

	@Override
	public boolean update(Object obj) {
		Reservation d = (Reservation)obj;
		int x = 0;
		Connection con;
		
		try {
			con = JDBCUtil.getOracleConnection(); 
			
			PreparedStatement ps = con.prepareStatement(UPDATE);
			ps.setInt(1,d.getId());
			 ps.setInt(2, d.getPassengerId());
	          ps.setInt(3, d.getTrainId());
	          ps.setDate(4, (java.sql.Date) d.getReservationDate());
	          ps.setDate(5, (java.sql.Date) d.getJourneyDate());
	          ps.setString(6, d.getStatusOfReservation());
	          ps.setString(7, d.getPnrNo());
	          ps.setString(8, d.getSrcStation());
	          ps.setString(9, d.getDestStation());
					
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}		
		//JDBCUtil.cleanup(ps, con);
		return x==1;
	}

	@Override
	public List<Object> getAll() {
		Connection con = null;
		PreparedStatement stmt = null;
		List<Object> list = new ArrayList<>();
		
		try {
			con = JDBCUtil.getOracleConnection();
			stmt = con.prepareStatement(FIND_ALL);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				Reservation c = new Reservation();
				c.setId(rs.getInt("id"));
				c.setPassengerId(rs.getInt("passeng_id"));
				c.setTrainId(rs.getInt("train_id"));
				c.setReservationDate(rs.getDate("reservationdate"));//date converted into string
				c.setJourneyDate(rs.getDate("reservationdate"));
				c.setStatusOfReservation(rs.getString("status"));
				c.setPnrNo(rs.getString("pnr_no"));
				c.setSrcStation(rs.getString("srcstation"));
				c.setDestStation(rs.getString("deststation"));
				
				list.add(c);
			}
		} catch (SQLException e) {
			// e.printStackTrace();
			throw new RuntimeException(e);
		} finally {			
		}		
		return list;
	}

	@Override
	public Reservation getReservationById(int id) throws SQLException {
		Reservation c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
			ps.setInt(1, id);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Reservation();
				c.setId(rs.getInt("id"));
				c.setPassengerId(rs.getInt("passeng_id"));
				c.setTrainId(rs.getInt("train_id"));
				c.setReservationDate(rs.getDate("reservationdate"));//date converted into string
				c.setJourneyDate(rs.getDate("reservationdate"));
				c.setStatusOfReservation(rs.getString("status"));
				c.setPnrNo(rs.getString("pnr_no"));		
				c.setSrcStation(rs.getString("srcstation"));
				c.setDestStation(rs.getString("deststation"));
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;
	}

	@Override
	public Reservation getReservationByPnrNo(String pnrNo) {
		Reservation c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_PNRNO);
			ps.setString(1, pnrNo);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Reservation();
				c.setId(rs.getInt("id"));
				c.setPassengerId(rs.getInt("passeng_id"));
				c.setTrainId(rs.getInt("train_id"));
				c.setReservationDate(rs.getDate("reservationdate"));//date converted into string
				c.setJourneyDate(rs.getDate("reservationdate"));
				c.setStatusOfReservation(rs.getString("status"));
				c.setPnrNo(rs.getString("pnr_no"));	
				c.setSrcStation(rs.getString("srcstation"));
				c.setDestStation(rs.getString("deststation"));
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;
	}

	@Override
	public List<Reservation> getReservationByPassengerId(int passengerId) {
		Reservation c = null;
		ResultSet rs = null;
		List list = new ArrayList<>();
		try {			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_PASSENGERID);
			ps.setInt(1, passengerId);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Reservation();
				c.setId(rs.getInt("id"));
				c.setPassengerId(rs.getInt("passeng_id"));
				c.setTrainId(rs.getInt("train_id"));
				c.setReservationDate(rs.getDate("reservationdate"));//date converted into string
				c.setJourneyDate(rs.getDate("reservationdate"));
				c.setStatusOfReservation(rs.getString("status"));
				c.setPnrNo(rs.getString("pnr_no"));
				c.setSrcStation(rs.getString("srcstation"));
				c.setDestStation(rs.getString("deststation"));
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

	@Override
	public List<Reservation> getReservationByTrainId(int trainId) {
		Reservation c = null;
		ResultSet rs = null;
		List list = new ArrayList<>();
		try {			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_TRAINID);
			ps.setInt(1, trainId);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Reservation();
				c.setId(rs.getInt("id"));
				c.setPassengerId(rs.getInt("passeng_id"));
				c.setTrainId(rs.getInt("train_id"));
				c.setReservationDate(rs.getDate("reservationdate"));//date converted into string
				c.setJourneyDate(rs.getDate("reservationdate"));
				c.setStatusOfReservation(rs.getString("status"));
				c.setPnrNo(rs.getString("pnr_no"));
				c.setSrcStation(rs.getString("srcstation"));
				c.setDestStation(rs.getString("deststation"));
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

	@Override
	public List<Reservation> getReservationByReservationDate(Date reservationDate) {
		Reservation c = null;
		ResultSet rs = null;
		List list = new ArrayList<>();
		try {			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_RESERVATIONDATE);
			ps.setDate(1, (java.sql.Date) reservationDate);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Reservation();
				c.setId(rs.getInt("id"));
				c.setPassengerId(rs.getInt("passeng_id"));
				c.setTrainId(rs.getInt("train_id"));
				c.setReservationDate(rs.getDate("reservationdate"));//date converted into string
				c.setJourneyDate(rs.getDate("reservationdate"));
				c.setStatusOfReservation(rs.getString("status"));
				c.setPnrNo(rs.getString("pnr_no"));
				c.setSrcStation(rs.getString("srcstation"));
				c.setDestStation(rs.getString("deststation"));
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

	@Override
	public List<Reservation> getReservationByJourneyDate(Date journeyDate) {
		Reservation c = null;
		ResultSet rs = null;
		List list = new ArrayList<>();
		try {			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_JOURNEYDATE);
			ps.setDate(1, (java.sql.Date) journeyDate);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Reservation();
				c.setId(rs.getInt("id"));
				c.setPassengerId(rs.getInt("passeng_id"));
				c.setTrainId(rs.getInt("train_id"));
				c.setReservationDate(rs.getDate("reservationdate"));//date converted into string
				c.setJourneyDate(rs.getDate("reservationdate"));
				c.setStatusOfReservation(rs.getString("status"));
				c.setPnrNo(rs.getString("pnr_no"));
				c.setSrcStation(rs.getString("srcstation"));
				c.setDestStation(rs.getString("deststation"));
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

	@Override
	public List<Reservation> getReservationByStatusOfReservation(String status) {
		Reservation c = null;
		ResultSet rs = null;
		List list = new ArrayList<>();
		try {			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_STATUS);
			ps.setString(1, status);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Reservation();
				c.setId(rs.getInt("id"));
				c.setPassengerId(rs.getInt("passeng_id"));
				c.setTrainId(rs.getInt("train_id"));
				c.setReservationDate(rs.getDate("reservationdate"));//date converted into string
				c.setJourneyDate(rs.getDate("reservationdate"));
				c.setStatusOfReservation(rs.getString("status"));
				c.setPnrNo(rs.getString("pnr_no"));
				c.setSrcStation(rs.getString("srcstation"));
				c.setDestStation(rs.getString("deststation"));
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

	

}
