package com.bookmytkt.Newdao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.bookmytkt.InterfaceDao.TrainTableDao;
import com.bookmytkt.dto.TrainFrequency;
import com.bookmytkt.dto.Traintable;

import JDBCUtil.JDBCUtil;

public class TrainTableDaoImpl implements TrainTableDao{
	
	private static final String FINDSEQNEXTVAL = "SELECT traintableseq.nextval from dual";

	
	private static final String DELETE = "delete from traintable_res  where id=?";
	private static final String FIND_ALL = "SELECT * FROM traintable_res ORDER BY id";
	private static final String FIND_BY_ID = "SELECT * FROM traintable_res WHERE id=?";
	private static final String FIND_BY_TRAINIDANDNAME = "select id ,trainname from traintable_Res";


	private static final String FIND_BY_TRAINNAME = "SELECT * FROM traintable_res WHERE trainname=?";
	private static final String FIND_BY_TRAINNO = "SELECT * FROM traintable_res WHERE train_no=?";
	private static final String FIND_BY_FARE = "SELECT * FROM traintable_res WHERE fare=?";
	private static final String FIND_BY_NOOFSEATS = "SELECT * FROM traintable_res WHERE noofseats=?";
	
	private static final String INSERT = "INSERT INTO traintable_res VALUES(?, ?,?,?,?)";
	private static final String UPDATE = "UPDATE traintable_res SET trainname=?,train_no=?,fare=? ,noofseats=? WHERE id=?";
	
	
public static boolean updateSeats(int noOfBookedTkt, int trainId)  {	

		boolean cond = false;
		Connection con = null;
		PreparedStatement ps; 
		Statement stmt;
		try {
			 con = JDBCUtil.getOracleConnection(); 
			stmt = con.createStatement();			
			ResultSet rs = stmt.executeQuery(" select noofseats from traintable_Res where id=" + trainId);
			while (rs.next()) {
				int NoOfSeats = rs.getInt("noofseats");
				int SeatsRem = NoOfSeats - noOfBookedTkt;
				if (SeatsRem > 0) {
					 ps = con.prepareStatement("update traintable_res set noofseats=? where id=?");
					ps.setInt(1, SeatsRem);
					ps.setInt(2, trainId);

					ps.executeUpdate();
					// TODO:Make use of ternary operator over here and other places as much as
					// possible
					cond = true;
				} else
					cond = false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cond;
	}
	
	
	public List<Object> getTrainByTrainIdAndName() {
		Traintable c = null;
		ResultSet rs = null;
		List<Object> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_TRAINIDANDNAME);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Traintable();
				 c.setId(rs.getInt("id"));
				 c.setTrainName(rs.getString("trainname"));
				
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;	
	}
	
	public static boolean CheckSeatAvailability(int TrainNo, int NoOfTkt) {

		Connection con;		
		Statement stmt;
		try {
			con = JDBCUtil.getOracleConnection();
			stmt = con.createStatement();			
			ResultSet rs = stmt.executeQuery(
					"select train_no,    trainname ,noofseats,arrivtime,depttime "
					+ "from TrainTable_Res x inner join TrainRoute_res y "
					+ "on x.id = y.trainid  where train_no ="
							+ TrainNo);
			while (rs.next()) {				
				// TODO:use column names instead of column positions everywhere
				if (NoOfTkt > rs.getInt("noofseats")) {				
					return false;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return true;
	}
	
	@Override
	public int findNextSeqVal() {
		int x = 0;
		Connection con = null;
		try {
			con = JDBCUtil.getOracleConnection();
			  PreparedStatement stmt;
			 stmt = con.prepareStatement(FINDSEQNEXTVAL);
			  ResultSet rs = stmt.executeQuery();
			  while(rs.next()) {
				  x = rs.getInt(1);
				  
			  }
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x;

	}
	
	@Override
	public int add(Object obj) {
		int x = 0;
		Traintable d = (Traintable)obj;
	
	  	  Connection con;
			try {
				con = JDBCUtil.getOracleConnection();
				x = findNextSeqVal();
					
				  PreparedStatement ps = con.prepareStatement(INSERT);		
				  ps.setInt(1, x);
			          ps.setString(2,d.getTrainName());
			          ps.setInt(3, d.getTrainNo());
			          ps.setFloat(4, d.getFare());
			          ps.setInt(5, d.getNoOfSeats());
			          ps.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	  	return x;
	}
	@Override
	public boolean delete(int id) {
		int x = 0;
		try {
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(DELETE);
			ps.setInt(1, id);
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x==1;
	}
	@Override
	public boolean update(Object obj) {
		Traintable d = (Traintable)obj;
		int x = 0;
		Connection con;
		
		try {
			con = JDBCUtil.getOracleConnection(); 
			
			PreparedStatement ps = con.prepareStatement(UPDATE);
			 ps.setString(1,d.getTrainName());
	          ps.setInt(2, d.getTrainNo());
	          ps.setFloat(3, d.getFare());
	          ps.setInt(4, d.getNoOfSeats());
	          ps.setInt(5, d.getId());
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}		
		//JDBCUtil.cleanup(ps, con);
		return x==1;	
	}
	@Override
	public List<Object> getAll() {
		Traintable c = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<Object> list = new ArrayList<>();
		
		try {
			con = JDBCUtil.getOracleConnection();
			stmt = con.prepareStatement(FIND_ALL);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				c = new Traintable();
				 c.setId(rs.getInt("id"));
				c.setTrainName(rs.getString("trainname"));
				c.setTrainNo(rs.getInt("train_no"));;
				c.setFare(rs.getFloat("fare"));
				c.setNoOfSeats(rs.getInt("noofseats"));
				
				list.add(c);
			}
		} catch (SQLException e) {
			// e.printStackTrace();
			throw new RuntimeException(e);
		} finally {			
		}		
		return list;
	}
	@Override
	public Traintable getTrainTableById(int id) throws SQLException {
		Traintable c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
			ps.setInt(1, id);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Traintable();
				 c.setId(rs.getInt("id"));
				 c.setTrainName(rs.getString("trainname"));
					c.setTrainNo(rs.getInt("train_no"));;
					c.setFare(rs.getFloat("fare"));
					c.setNoOfSeats(rs.getInt("noofseats"));
					
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;		
	}
	@Override
	public List<Traintable> getTrainTableByTrainName(String trainName) {
		Traintable c = null;
		ResultSet rs = null;
		List<Traintable> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_TRAINNAME);
			ps.setString(1, trainName);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Traintable();
				 c.setId(rs.getInt("id"));
				 c.setTrainName(rs.getString("trainname"));
					c.setTrainNo(rs.getInt("train_no"));;
					c.setFare(rs.getFloat("fare"));
					c.setNoOfSeats(rs.getInt("noofseats"));
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;	
	}
	@Override
	public List<Traintable> getTrainTableByTrainNo(int trainNo) {
		Traintable c = null;
		ResultSet rs = null;
		List<Traintable> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_TRAINNO);
			ps.setInt(1, trainNo);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Traintable();
				 c.setId(rs.getInt("id"));
				 c.setTrainName(rs.getString("trainname"));
					c.setTrainNo(rs.getInt("train_no"));;
					c.setFare(rs.getFloat("fare"));
					c.setNoOfSeats(rs.getInt("noofseats"));
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;	
	}
	@Override
	public List<Traintable> getTrainTableByTrainFare(Float trainFare) {
		Traintable c = null;
		ResultSet rs = null;
		List<Traintable> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_FARE);
			ps.setFloat(1, trainFare);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Traintable();
				 c.setId(rs.getInt("id"));
				 c.setTrainName(rs.getString("trainname"));
					c.setTrainNo(rs.getInt("train_no"));;
					c.setFare(rs.getFloat("fare"));
					c.setNoOfSeats(rs.getInt("noofseats"));
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;	
	}
	@Override
	public List<Traintable> getTrainTableByNoOfSeats(int noOfSeats) {
		Traintable c = null;
		ResultSet rs = null;
		List<Traintable> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_NOOFSEATS);
			ps.setInt(1, noOfSeats);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Traintable();
				 c.setId(rs.getInt("id"));
				 c.setTrainName(rs.getString("trainname"));
					c.setTrainNo(rs.getInt("train_no"));;
					c.setFare(rs.getFloat("fare"));
					c.setNoOfSeats(rs.getInt("noofseats"));
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;	
	}
	
	

}
