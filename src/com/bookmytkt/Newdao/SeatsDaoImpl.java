package com.bookmytkt.Newdao;

import com.bookmytkt.InterfaceDao.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bookmytkt.InterfaceDao.SeatsDao;

import com.bookmytkt.dto.*;

import JDBCUtil.JDBCUtil;

public class SeatsDaoImpl implements SeatsDao{
	
	private static final String FINDSEQNEXTVAL = "SELECT seatseq.nextval from dual";

	private static final String DELETE = "delete from seats  where id=?";
	private static final String FIND_ALL = "SELECT * FROM seats ORDER BY id";
	private static final String FIND_BY_ID = "SELECT * FROM seats WHERE id=?";
	private static final String FIND_BY_DATEOfBOOKING = "SELECT * FROM seats WHERE dateofbooking=?";

	private static final String FIND_BY_TRAINNO = "SELECT * FROM seats WHERE trainno=?";
	private static final String FIND_BY_SEATBOOKED = "SELECT * FROM seats WHERE seatbooked=?";

	
	private static final String INSERT = "INSERT INTO seats VALUES(?, ?,?,?)";
	private static final String UPDATE = "UPDATE seats SET dateofbooking=?,trainno=?,seatbooked=? WHERE id=?";
	
	
	@Override
	public int findNextSeqVal() {
		int x = 0;
		Connection con = null;
		try {
			con = JDBCUtil.getOracleConnection();
			  PreparedStatement stmt;
			 stmt = con.prepareStatement(FINDSEQNEXTVAL);
			  ResultSet rs = stmt.executeQuery();
			  while(rs.next()) {
				  x = rs.getInt(1);
				  
			  }
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x;
	}
	

	@Override
	public int add(Object obj) {
		int x = 0;
		Seats d = (Seats)obj;
	
	  	  Connection con;
			try {
				con = JDBCUtil.getOracleConnection();
				x = findNextSeqVal();
					
				  PreparedStatement ps = con.prepareStatement(INSERT);	
				  ps.setInt(1, x);
			          ps.setString(2,d.getDateOfBooking());
			          ps.setInt(3, d.getTrainNo());
			          ps.setInt(4,d.getSeatsBooked());   			          
			           ps.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	  	return x;
	}

	@Override
	public boolean delete(int id) {
		int x = 0;
		try {
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(DELETE);
			ps.setInt(1, id);
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x==1;
	}

	@Override
	public boolean update(Object obj) {
		Seats d = (Seats)obj;
		int x = 0;
		Connection con;
		
		try {
			con = JDBCUtil.getOracleConnection(); 
			
			PreparedStatement ps = con.prepareStatement(UPDATE);
			      ps.setString(1,d.getDateOfBooking());
		          ps.setInt(2, d.getTrainNo());
		          ps.setInt(3,d.getSeatsBooked()); 
		          ps.setInt(4, d.getId());
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}		
		//JDBCUtil.cleanup(ps, con);
		return x==1;
	}

	@Override
	public List<Object> getAll() {
		Connection con = null;
		PreparedStatement stmt = null;
		List<Object> list = new ArrayList<>();
		
		try {
			con = JDBCUtil.getOracleConnection();
			stmt = con.prepareStatement(FIND_ALL);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				Seats c = new Seats();
				c.setId(rs.getInt("id"));
				c.setDateOfBooking(rs.getString("dateofbooking"));
				c.setTrainNo(rs.getInt("trainno"));
				c.setSeatsBooked(rs.getInt("seatbooked"));
				list.add(c);
			}
		} catch (SQLException e) {
			// e.printStackTrace();
			throw new RuntimeException(e);
		} finally {			
		}		
		return list;
	}

	@Override
	public  Seats getSeatsById(int id) throws SQLException {
		Seats c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
			ps.setInt(1, id);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Seats();
				 c.setId(rs.getInt("id"));
				 c.setDateOfBooking(rs.getString("dateofbooking"));
				 c.setTrainNo(rs.getInt("trainno"));
				 c.setSeatsBooked(rs.getInt("seatbooked"));
					
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;
	}

	
	@Override
	public List<Seats> getSeatsByDateOfBooking(String dateOfBooking) {
		Seats c = null;
		ResultSet rs = null;
		List<Seats> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_DATEOfBOOKING);
			ps.setString(1, dateOfBooking);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Seats();
				 c.setId(rs.getInt("id"));
				 c.setDateOfBooking(rs.getString("dateofbooking"));
				 c.setTrainNo(rs.getInt("trainno"));
				 c.setSeatsBooked(rs.getInt("seatbooked"));
				
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

	@Override
	public List<Seats> getSeatsByTrainNo(int trainNo) {
		Seats c = null;
		ResultSet rs = null;
		List<Seats> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_TRAINNO);
			ps.setInt(1, trainNo);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Seats();
				 c.setId(rs.getInt("id"));
				 c.setDateOfBooking(rs.getString("dateofbooking"));
				 c.setTrainNo(rs.getInt("trainno"));
				 c.setSeatsBooked(rs.getInt("seatbooked"));
				
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

	@Override
	public List<Seats> getSeatsBySeatsBooked(int seatsBooked) {
		Seats c = null;
		ResultSet rs = null;
		List<Seats> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_SEATBOOKED);
			ps.setInt(1, seatsBooked);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Seats();
				 c.setId(rs.getInt("id"));
				 c.setDateOfBooking(rs.getString("dateofbooking"));
				 c.setTrainNo(rs.getInt("trainno"));
				 c.setSeatsBooked(rs.getInt("seatbooked"));
				
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}
}
