package com.bookmytkt.Newdao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.print.Doc;

import com.bookmytkt.InterfaceDao.DocIdDao;
import com.bookmytkt.dto.*;

import JDBCUtil.JDBCUtil;

public class DocIdDaoImpl implements DocIdDao{
	
	
	private static final String FINDSEQNEXTVAL = "SELECT docidseq.nextval from dual";

	
	private static final String DELETE = "delete from docid_res  where id=?";
	private static final String FIND_ALL = "SELECT * FROM docid_res ORDER BY id";
	private static final String FIND_BY_ID = "SELECT * FROM docid_res WHERE id=?";
	private static final String FIND_BY_TYPE = "SELECT * FROM docid_res WHERE type=?";
	private static final String INSERT = "INSERT INTO docid_res VALUES(?, ?)";
	private static final String UPDATE = "UPDATE docid_res SET type=? WHERE id=?";
	
	@Override
	public int findNextSeqVal() {
		int x = 0;
		Connection con = null;
		try {
			con = JDBCUtil.getOracleConnection();
			  PreparedStatement stmt;
			 stmt = con.prepareStatement(FINDSEQNEXTVAL);
			  ResultSet rs = stmt.executeQuery();
			  while(rs.next()) {
				  x = rs.getInt(1);
				  
			  }
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x;
	}

	@Override
	public int add(Object obj) {
		int x = 0;
		DocId d = (DocId)obj;
	  	  Connection con;
			try {
				con = JDBCUtil.getOracleConnection();
			x = findNextSeqVal();
					
				  PreparedStatement ps = con.prepareStatement(INSERT);	
				  ps.setInt(1, x);
			          ps.setString(2, d.getTypeOfId());			          	
			           ps.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	  	return x;
	}

	@Override
	public boolean delete(int id) {
		int x = 0;
		try {
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(DELETE);
			ps.setInt(1, id);
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x==1;
	}

	@Override
	public boolean update(Object obj) {
		
		DocId d = (DocId)obj;
		int x = 0;
		Connection con;
		
		try {
			con = JDBCUtil.getOracleConnection(); 
			
			PreparedStatement ps = con.prepareStatement(UPDATE);
			ps.setInt(2, d.getId());
			ps.setString(1, d.getTypeOfId());			
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}		
		//JDBCUtil.cleanup(ps, con);
		return x==1;
	}

	@Override
	public List<Object> getAll() {
		Connection con = null;
		PreparedStatement stmt = null;
		List<Object> list = new ArrayList<>();
		
		try {
			con = JDBCUtil.getOracleConnection();
			stmt = con.prepareStatement(FIND_ALL);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				DocId c = new DocId();
				c.setId(rs.getInt("id"));
				c.setTypeOfId(rs.getString("type"));				
				list.add(c);
			}
		} catch (SQLException e) {
			// e.printStackTrace();
			throw new RuntimeException(e);
		} finally {			
		}		
		return list;
	}

	@Override
	public List<Object> getIdByType(String type) {
		DocId c = null;
		ResultSet rs = null;
		List<Object> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_TYPE);
			ps.setString(1, type);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new DocId();
				 c.setId(rs.getInt("id"));
				c.setTypeOfId(rs.getString("type"));
				
				list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;
	}

	@Override
	public DocId getdocById(int id) {
		DocId c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
			ps.setInt(1, id);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new DocId();
				 c.setId(rs.getInt("id"));
					c.setTypeOfId(rs.getString("type"));
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;
	}

	
	

	
	

}
