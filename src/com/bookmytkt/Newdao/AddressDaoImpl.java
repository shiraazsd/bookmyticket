package com.bookmytkt.Newdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bookmytkt.InterfaceDao.AddressDao;
import com.bookmytkt.dto.Address;
import com.bookmytkt.dto.Billing;
import com.bookmytkt.dto.User;

import JDBCUtil.JDBCUtil;

public class AddressDaoImpl implements AddressDao{
	 
    private static final String DELETE = "delete from address_res  where id=?";
	private static final String FIND_ALL = "SELECT * FROM address_res ORDER BY id";
	private static final String FIND_BY_ID = "SELECT * FROM address_res WHERE id=?";
	private static final String FIND_BY_ADDRESS = "SELECT * FROM address_res WHERE address=?";
	private static final String FIND_BY_CITY_ID = "SELECT * FROM address_res WHERE city_id=?";
	private static final String FIND_BY_ADDRESSCITYANDID = "select id ,address from address_Res";
	private static final String FINDSEQNEXTVAL = "SELECT addressseq.nextval from dual";



	
	private static final String INSERT = "INSERT INTO address_res VALUES(?, ?, ?)";
	private static final String UPDATE = "UPDATE address_res SET address=?, city_id=? WHERE id=?";
	
	
	@Override
	public int findNextSeqVal() {
		int x = 0;
		Connection con = null;
		try {
			con = JDBCUtil.getOracleConnection();
			  PreparedStatement stmt;
			 stmt = con.prepareStatement(FINDSEQNEXTVAL);
			  ResultSet rs = stmt.executeQuery();
			  while(rs.next()) {
				  x = rs.getInt(1);
				  
			  }
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x;
	}
	   
	
	@Override
	public int add(Object obj) {
		
		int x = 0;
		Address d = (Address)obj;
		
  	  Connection con;
		try {
			con = JDBCUtil.getOracleConnection();
			x = findNextSeqVal();		
			  PreparedStatement ps = con.prepareStatement(INSERT);
		  		 // Statement stmt = con.createStatement();
		         ps.setInt(1, x);
			 
		          ps.setString(2, d.getAddress());
		          ps.setInt(3, d.getCityId());		
		         ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
  	return x;
	}
	
	@Override
	public boolean delete(int id) {
		int x = 0;
		try {
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(DELETE);
			ps.setInt(1, id);
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return x==1;
	}
	
	@Override
	public boolean update(Object obj) {
		Address d = (Address)obj;
		int x = 0;
		Connection con;
		
		try {
			con = JDBCUtil.getOracleConnection(); 
			
			PreparedStatement ps = con.prepareStatement(UPDATE);
		    ps.setString(1,d.getAddress());
	          ps.setInt(2, d.getCityId());  
	         
	          ps.setInt(3, d.getId());
			x = ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}		
		//JDBCUtil.cleanup(ps, con);
		return x==1;	
	}
	@Override
	public List<Object> getAll() {
		Address c = null;
		Connection con = null;
		PreparedStatement stmt = null;
		List<Object> list = new ArrayList<>();
		
		try {
			con = JDBCUtil.getOracleConnection();
			stmt = con.prepareStatement(FIND_ALL);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				c = new Address();
				 c.setId(rs.getInt("id"));
				c.setAddress(rs.getString("address"));
				c.setCityId(rs.getInt("city_id"));			
				
				list.add(c);
			}
		} catch (SQLException e) {
			// e.printStackTrace();
			throw new RuntimeException(e);
		} finally {			
		}		
		return list;
	}
	@Override
	public Address getAddress(int addressId) {
		Address c = null;
		ResultSet rs = null;
		
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ID);
			ps.setInt(1, addressId);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Address();
				 c.setId(rs.getInt("id"));
				 c.setAddress(rs.getString("address"));
				 c.setCityId(rs.getInt("city_id"));	
					
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return c;	
	}
	@Override
	public List<Address> getAddressByaddress(String address) {
		Address c = null;
		ResultSet rs = null;
		List<Address> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_ADDRESS);
			ps.setString(1, address);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Address();
				 c.setId(rs.getInt("id"));
				
					 c.setAddress(rs.getString("address"));
					 c.setCityId(rs.getInt("city_id"));
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;		
	}
	@Override
	public List<Address> getAddressByCityId(int cityId) {
		Address c = null;
		ResultSet rs = null;
		List<Address> list = new ArrayList<>();
		try {
			
			Connection con = JDBCUtil.getOracleConnection(); 
			PreparedStatement ps = con.prepareStatement(FIND_BY_CITY_ID);
			ps.setInt(1, cityId);
			 rs = ps.executeQuery();
			// System.out.println(rs.next());
			while(rs.next()) {
				
				 c = new Address();
				 c.setId(rs.getInt("id"));
				
					 c.setAddress(rs.getString("address"));
					 c.setCityId(rs.getInt("city_id"));
					list.add(c);
			}		
		} catch (Exception e) {
			// TODO: handle exception
		}			
		return list;		
	}

	 

}
